
# Telecom Offboarding Utility

This applet is a custom made series of scripts designed to facilitate SelectQuote Telecom
with offboarding.

For a full flowchart visit:

https://lucid.app/lucidchart/e9af4028-f533-40e7-9851-7124b5ae25e1/edit?viewport_loc=-113%2C1630%2C2466%2C1244%2C0_0&invitationId=inv_14469fbe-fe8c-4c8f-91e4-1f97f5cf614d#

NOTE: The Amazon Connect features are not currently implemented.

- Author: Joshua Hurd
- Contact: Joshua Hurd, James Porteous, Alan Noble
## Features

- Offboarding from the 6 primary Five9 domains that Telecom manages
- Full email reporting of steps taken
- Ability to offboard by employee ID, name, domain, and username.
- Runs in Powershell using Google and Five9 API platforms


## Technical Details

The offboarding tool tracks offboarding tasks by using an aptly named "Super Record."

When run, the tool will check a provided Google Sheet for requested offboarding with the 
following expected column headers:

- Employee Name
- Employee ID
- Username
- Domain
- Email Address

Depending on the details provided in the Google Sheet, different steps will be taken: 

- If an Employee Name is provided, then a search will be done using the name. This method
    is not always accurate due to common typos or hyphens, along with some employees having
    the same name. If multiple matches are found for the same name, the tool will not
    continue and alert the provided Email Address to review the offboarding manually.
    Accounts matched by Employee Name will only be completed in Five9 and any related
    contact records and other data will not be changed.
- If Employee ID is provided, a search will be done across all Google Contact Sheets, 
    unless a domain is specified. Any Google Record matching the employee ID provided
    will be added to the Super Record. Once all matches are gathered, each Super
    Record will be executed and have any provided data modifed, removed, or disabled. If
    no Employee ID's are matched, it will fallback to Employee Name if provided.
- If a Username is specified, all Google Contact Sheets will be searched for a matching
    username. If none are found, it will fallback to a Five9 only search.
- If a Domain is specified, all searches will stay within the provided Domain only.
- Email Address is required for any action to be taken. All reporting will go to the
    provided Email.

## Google OAuth

This tool uses OAuth 2.0 to access Google API commands.

Google OAuth provies a non-expiring Refresh Token with predefined scopes, in this case,
GSheets and GMail.


Generating API Oauth tokens with Google is a bit complicated, as typically it would
require an HTTP listener for a Refresh Token. Included in this tool in the "Secure" folder
is a Powershell script called "createGoogleSecret.ps1". This tool will handle all of the
authentication steps to aquire this Refresh Token, however the result will look like it
failed because there is no HTTP listener. Instead, the Refresh Token is returned in the
URL bar of the provided "error" page.

It is also important to note that login credentials for telecom_ps@selectquote.com will be
needed, or an alternative service account to be used for this project. These credentials
are provided on a need to know basis. Once credentials are obtained, navigate to 
https://console.cloud.google.com/apis/credentials and either use existing an existing
OAuth 2.0 Client ID or generate a new one. Set the Client ID to be for a Web Application
and the authorized redirect URIs must include http://localhost/oauth2callback and
http://localhost/. Once this is set, you can copy and paste the Client ID and Client secret
into the prompt for the createGoogleSecret script.

Once the Refresh Token is obtained, it will be stored in an encrypted file in the "Secure"
folder called "Google_OAuth.txt" A second file called "Google_AccessToken.txt" will be
created to keep track of the Access Token created from the Refresh Token. This Access Token
expires after one hour and will be automatically renewed by the tool if needed.

Please note that the encrypted key for these tokens is computer specific and cannot be
copied to another machine or user account. A new file will need to be generated if moving
machines.

## Five9 Authentication

Five9 authentication requires a provided BASIC username and password. This tool automatically
translates provided raw text into BASIC and will store them in a local encrypted file under
the "Secure" folder. The first time the script is run, it will prompt for these credentials
in the following order:

- Senior
- UHC (Mirror)
- CCA
- SQAH
- Health
- Life

Once these credentials are provided, it will not ask for them again. To reset the credentails
stored for a certain domain, navigate to the "Secure" folder and delete the appropriate file.
For example, to reset Senior, delete "Cred_Senior.xml"

Please note that the encrypted key for these tokens is computer specific and cannot be
copied to another machine or user account. A new file will need to be generated if moving
machines.

## Google Steps

Due to the fact that a majority of Telecom's data is kept within Google Sheets, many
parts of the offboarding process involve modifying these sheets, called Contact Sheets.


The major Google API requests are as follows:

- Retrieval of data for offboard requests
- Retrieval of data for each contact sheet for each domain
- Retrieval of data for offboarded accounts pending deletion
- Creation of data for offboarded accounts pending deletion
- Creation of data for offboarded accounts in the various contact sheets as appropriate
- Removal of data for offboarded accounts Station ID's
- Removal of data for offboarded accounts Agent Mapping
- Email reporting


When removing data from Gsheets, the offboard tool utilizes a bulk remove command
by tracking the current index of the data that was retrieved. Because retireving data
strips it of the headers, all indexes must have 2 rows added to be accurate to Gsheet.

Creation of data uses the Append API call which will add data to the first empty cell
found within a specified range. This command is somewhat confusing, but to keep all
data starting from the A column, it is important to keep all append calls within the
range A:A on the sheet.

The Google API calls also include a set of prewritten emails to send to the provided
email address with a report of data stored in the Super Record. Typical GMail API use
requires enabled "Less secure apps" for SMTP access. However, this tool uses OAuth
authorization headers which requires a Base64 string in MIME RFC 2822 formatting. The
easiest to generate new emails is to create one in Gmail and open the original, which
is displayed in MIME.



## Five9 Steps

Five9 API access depends on establishing a connection using AdminWebService first and
then providing commands to that connection. The connection is Domain specific, meaning
that if the tool logs into Senior, all subsequent Five9 API calls will be effective
only in the Senior domain. To switch domains, the AdminWebService will need to be
re-established.

The major API calls this tool uses are as follows:

- AdminWebService (Open Connection)
- Generate and retrieve reports
- Add and remove Campaign DNIS
- Retrieve and remove Speed Dial
- Retrieve, modify, and remove User
- Retreive and remove Contact Record
- Remove Agent Group Member

The retrieval of reports using Five9 API requires that a custom report already be created.
The API will run the report, and then retrieve it using the unique report ID generated. For
this tool, all reports are stored under the folder "API Reports" for each domain.

Because Five9 Contact Records should match the separately created Google Contact Sheets,
it is important that matching Contact Records are deleted before the Google Contact Sheet
has its copy removed. This is because the Five9 contact records are not openly searchable,
and it's required to have a key to search for. If the Google Contact Record is deleted
before the Five9 record, it is possible that the Five9 deletion my fail unsafely and remove
the keys needed to search by.

## Troubleshooting

The most likely cause of errors in this project is related to authentication.

Double check that the Google Refresh Token hasn't been dropped for some reason.

Also verify that none of the Five9 service accounts used haven't been locked, had
their passwords expire, or had their passwords changed.

**Access to modify these settings will require access to the machine running the Powershell
script.**

For any further issues, please report them to the listed contacts at the top of this
ReadMe. If you are the poor soul who has been assigned to fixing a major crash with
this project, all logs are stored in the "Logging" folder. The error reporting for
Powershell will usually be in a block of text specifying the line and column of where
the error ocurred. The most likely cause for errors beyond authentication issues is
changes or updates to the Google or Five9 API.

If you are a coding genuis in the distant future looking to add new Five9 domains to this
offboarding tool, there are a lot of areas that reference domains specifically. Generally,
just copy and pasting the same formatting with the new domain should be all that's needed.
Don't forget to add new credentials, reports, and access to the new domain!

You can refer to details regarding Five9 Powershell API here:
https://github.com/sqone2/PSFive9Admin

You can refer to details regarding GSheet API here:
https://developers.google.com/sheets/api

You can refer to details regarding GMail API here:
https://developers.google.com/gmail/api
