function Get-EmployeeIDMatches {
    param (
        [Parameter(Mandatory=$true)][string]$token,
        [Parameter(Mandatory=$true)][string]$EID,
        [Parameter(Mandatory=$false)]$SeniorData,
        [Parameter(Mandatory=$false)]$UHCData,
        [Parameter(Mandatory=$false)]$CCAData,
        [Parameter(Mandatory=$false)]$SQAHData,
        [Parameter(Mandatory=$false)]$HealthData,
        [Parameter(Mandatory=$false)]$LifeData
    )

    $matchList = New-Object System.Collections.ArrayList($null)

    #Check if a domain is specified
    $SeniorFilter = ($SeniorData | ForEach-Object {If ($_.Employee_ID -eq $EID) {$_}})
    $UHCFilter = ($UHCData | ForEach-Object {If ($_.Employee_ID -eq $EID) {$_}})
    $CCAFilter = ($CCAData | ForEach-Object {If ($_.Employee_ID -eq $EID) {$_}})
    $SQAHFilter = ($SQAHData | ForEach-Object {If ($_.Employee_ID -eq $EID) {$_}})
    $HealthFilter = ($HealthData | ForEach-Object {If ($_.Employee_ID -eq $EID) {$_}})
    $LifeFilter = ($LifeData | ForEach-Object {If ($_.Employee_ID -eq $EID) {$_}})

    ForEach ($Match in $SeniorFilter) {
        $MatchIndex = $SeniorData.IndexOf($Match) + 2
        $MatchUser = $Match.Agent_Username
        Write-Host "Matched $MatchUser to $EID in Senior!"
        [void]$matchList.add([PSCustomObject]@{
            'Username' = [string]$MatchUser
            'Name' = [string]$Match.Agent_Name
            'DNIS'= [string]$Match.Agent_DNIS
            'TempDNIS' = [string]$Match.Agent_Temp_DNIS
            'Extension' = [string]$Match.Agent_Extension
            'Campaign' = [string]$Match.Five9_Campaign_Group
            'Record' = [string]$Match.number1
            'Loopback' = [string]$Match.Loopback
            'Amazon' = [string]$Match.Connect
            'EID' = [string]$EID
            'Department' = [string]$Match.Department
            'Note'  = [string]$Match.Notes
            'Domain' = 'Senior'
            'Index' = $MatchIndex
        })
    }
    ForEach ($Match in $UHCFilter) {
        $MatchIndex = $UHCData.IndexOf($Match) + 2
        $MatchUser = $Match.Agent_Username
        Write-Host "Matched $MatchUser to $EID in UHC!"
        [void]$matchList.add([PSCustomObject]@{
            'Username' = [string]$MatchUser
            'Name' = [string]$Match.Agent_Name
            'DNIS'= [string]$Match.Agent_DNIS
            'TempDNIS' = [string]$Match.Agent_Temp_DNIS
            'Extension' = [string]$Match.Agent_Extension
            'Campaign' = [string]$Match.Five9_Campaign_Group
            'Record' = [string]$Match.number1
            'Loopback' = [string]$Match.Loopback
            'Amazon' = [string]$Match.Connect
            'EID' = [string]$EID
            'Department' = [string]$Match.Department
            'Note'  = [string]$Match.Notes
            'Domain' = 'UHC'
            'Index' = $MatchIndex
        })
    }
    ForEach ($Match in $CCAFilter) {
        $MatchIndex = $CCAData.IndexOf($Match) + 2
        $MatchUser = $Match.Agent_Username
        Write-Host "Matched $MatchUser to $EID in CCA!"
        [void]$matchList.add([PSCustomObject]@{
            'Username' = [string]$MatchUser
            'Name' = [string]$Match.'first_name'
            'DNIS'= [string]$Match.Agent_DNIS
            'TempDNIS' = [string]$Match.Agent_Temp_DNIS
            'Extension' = [string]$Match.Agent_Extension
            'Campaign' = [string]$Match.Five9_Campaign_Group
            'Record' = [string]$Match.number1
            'Loopback' = [string]$Match.Loopback
            'Amazon' = [string]$Match.Connect
            'EID' = [string]$EID
            'Department' = [string]$Match.Department
            'Note'  = [string]$Match.Notes
            'Domain' = 'CCA'
            'Index' = $MatchIndex
        })
    }
    ForEach ($Match in $SQAHFilter) {
        $MatchIndex = $SQAHData.IndexOf($Match) + 2
        $MatchUser = $Match.Agent_Username
        Write-Host "Matched $MatchUser to $EID in SQAH!"
        [void]$matchList.add([PSCustomObject]@{
            'Username' = [string]$MatchUser
            'Name' = [string]$Match.'first_name'
            'DNIS'= [string]$Match.Agent_DNIS
            'TempDNIS' = [string]$Match.Agent_Temp_DNIS
            'Extension' = [string]$Match.Agent_Extension
            'Campaign' = [string]$Match.Five9_Campaign_Group
            'Record' = [string]$Match.number1
            'Loopback' = [string]$Match.Loopback
            'Amazon' = [string]$Match.Connect
            'EID' = [string]$EID
            'Department' = [string]$Match.Department
            'Note'  = [string]$Match.Notes
            'Domain' = 'SQAH'
            'Index' = $MatchIndex
        })
    }
    ForEach ($Match in $HealthFilter) {
        $MatchIndex = $HealthData.IndexOf($Match) + 2
        $MatchUser = $Match.Agent_Username
        Write-Host "Matched $MatchUser to $EID in Health!"
        [void]$matchList.add([PSCustomObject]@{
            'Username' = [string]$MatchUser
            'Name' = [string]$Match.Agent_Name
            'DNIS'= [string]$Match.Agent_DNIS
            'TempDNIS' = [string]$Match.Agent_Temp_DNIS
            'Extension' = [string]$Match.Agent_Extension
            'Campaign' = [string]$Match.Five9_Campaign_Group
            'Record' = [string]$Match.number1
            'Loopback' = [string]$Match.Loopback
            'Amazon' = [string]$Match.Connect
            'EID' = [string]$EID
            'Department' = [string]$Match.Department
            'Note'  = [string]$Match.Notes
            'Domain' = 'Health'
            'Index' = $MatchIndex
        })
    }
    ForEach ($Match in $LifeFilter) {
        $MatchIndex = $LifeData.IndexOf($Match) + 2
        $MatchUser = $Match.Agent_Username
        Write-Host "Matched $MatchUser to $EID in Life!"
        [void]$matchList.add([PSCustomObject]@{
            'Username' = [string]$MatchUser
            'Name' = [string]$Match.'first_name'
            'DNIS'= [string]$Match.'agent ani'
            'TempDNIS' = [string]$Match.Agent_Temp_DNIS
            'Extension' = [string]$Match.Agent_Extension
            'Campaign' = [string]$Match.Five9_Campaign_Group
            'Record' = [string]$Match.number1
            'Loopback' = [string]$Match.Loopback
            'Amazon' = [string]$Match.Connect
            'EID' = [string]$EID
            'Department' = [string]$Match.Department
            'Note'  = [string]$Match.Notes
            'Domain' = 'Life'
            'Index' = $MatchIndex
        })
    }
    return $matchList
}

function Get-UsernameMatches {
    param (
        [Parameter(Mandatory=$true)][string]$token,
        [Parameter(Mandatory=$true)][string]$username,
        [Parameter(Mandatory=$false)]$SeniorData,
        [Parameter(Mandatory=$false)]$UHCData,
        [Parameter(Mandatory=$false)]$CCAData,
        [Parameter(Mandatory=$false)]$SQAHData,
        [Parameter(Mandatory=$false)]$HealthData,
        [Parameter(Mandatory=$false)]$LifeData
    )

    $matchList = New-Object System.Collections.ArrayList($null)

    #Check if a domain is specified
    $SeniorFilter = ($SeniorData | ForEach-Object {If ($_.Agent_Username -eq $username) {$_}})
    $UHCFilter = ($UHCData | ForEach-Object {If ($_.Agent_Username -eq $username) {$_}})
    $CCAFilter = ($CCAData | ForEach-Object {If ($_.Agent_Username -eq $username) {$_}})
    $SQAHFilter = ($SQAHData | ForEach-Object {If ($_.Agent_Username -eq $username) {$_}})
    $HealthFilter = ($HealthData | ForEach-Object {If ($_.Agent_Username -eq $username) {$_}})
    $LifeFilter = ($LifeData | ForEach-Object {If ($_.Agent_Username -eq $username) {$_}})

    ForEach ($Match in $SeniorFilter) {
        $MatchIndex = $SeniorData.IndexOf($Match) + 2
        $MatchUser = $Match.Agent_Username
        Write-Host "Matched $MatchUser to $username in Senior!"
        [void]$matchList.add([PSCustomObject]@{
            'Username' = [string]$MatchUser
            'Name' = [string]$Match.Agent_Name
            'DNIS'= [string]$Match.Agent_DNIS
            'TempDNIS' = [string]$Match.Agent_Temp_DNIS
            'Extension' = [string]$Match.Agent_Extension
            'Campaign' = [string]$Match.Five9_Campaign_Group
            'Record' = [string]$Match.number1
            'Loopback' = [string]$Match.Loopback
            'Amazon' = [string]$Match.Connect
            'EID' = [string]$Match.Employee_ID
            'Department' = [string]$Match.Department
            'Note'  = [string]$Match.Notes
            'Domain' = 'Senior'
            'Index' = $MatchIndex
        })
    }
    ForEach ($Match in $UHCFilter) {
        $MatchIndex = $UHCData.IndexOf($Match) + 2
        $MatchUser = $Match.Agent_Username
        Write-Host "Matched $MatchUser to $username in UHC!"
        [void]$matchList.add([PSCustomObject]@{
            'Username' = [string]$MatchUser
            'Name' = [string]$Match.Agent_Name
            'DNIS'= [string]$Match.Agent_DNIS
            'TempDNIS' = [string]$Match.Agent_Temp_DNIS
            'Extension' = [string]$Match.Agent_Extension
            'Campaign' = [string]$Match.Five9_Campaign_Group
            'Record' = [string]$Match.number1
            'Loopback' = [string]$Match.Loopback
            'Amazon' = [string]$Match.Connect
            'EID' = [string]$Match.Employee_ID
            'Department' = [string]$Match.Department
            'Note'  = [string]$Match.Notes
            'Domain' = 'UHC'
            'Index' = $MatchIndex
        })
    }
    ForEach ($Match in $CCAFilter) {
        $MatchIndex = $CCAData.IndexOf($Match) + 2
        $MatchUser = $Match.Agent_Username
        Write-Host "Matched $MatchUser to $username in CCA!"
        [void]$matchList.add([PSCustomObject]@{
            'Username' = [string]$MatchUser
            'Name' = [string]$Match.'first_name'
            'DNIS'= [string]$Match.Agent_DNIS
            'TempDNIS' = [string]$Match.Agent_Temp_DNIS
            'Extension' = [string]$Match.Agent_Extension
            'Campaign' = [string]$Match.Five9_Campaign_Group
            'Record' = [string]$Match.number1
            'Loopback' = [string]$Match.Loopback
            'Amazon' = [string]$Match.Connect
            'EID' = [string]$Match.Employee_ID
            'Department' = [string]$Match.Department
            'Note'  = [string]$Match.Notes
            'Domain' = 'CCA'
            'Index' = $MatchIndex
        })
    }
    ForEach ($Match in $SQAHFilter) {
        $MatchIndex = $SQAHData.IndexOf($Match) + 2
        $MatchUser = $Match.Agent_Username
        Write-Host "Matched $MatchUser to $username in SQAH!"
        [void]$matchList.add([PSCustomObject]@{
            'Username' = [string]$MatchUser
            'Name' = [string]$Match.'first_name'
            'DNIS'= [string]$Match.Agent_DNIS
            'TempDNIS' = [string]$Match.Agent_Temp_DNIS
            'Extension' = [string]$Match.Agent_Extension
            'Campaign' = [string]$Match.Five9_Campaign_Group
            'Record' = [string]$Match.number1
            'Loopback' = [string]$Match.Loopback
            'Amazon' = [string]$Match.Connect
            'EID' = [string]$Match.Employee_ID
            'Department' = [string]$Match.Department
            'Note'  = [string]$Match.Notes
            'Domain' = 'SQAH'
            'Index' = $MatchIndex
        })
    }
    ForEach ($Match in $HealthFilter) {
        $MatchIndex = $HealthData.IndexOf($Match) + 2
        $MatchUser = $Match.Agent_Username
        Write-Host "Matched $MatchUser to $username in Health!"
        [void]$matchList.add([PSCustomObject]@{
            'Username' = [string]$MatchUser
            'Name' = [string]$Match.Agent_Name
            'DNIS'= [string]$Match.Agent_DNIS
            'TempDNIS' = [string]$Match.Agent_Temp_DNIS
            'Extension' = [string]$Match.Agent_Extension
            'Campaign' = [string]$Match.Five9_Campaign_Group
            'Record' = [string]$Match.number1
            'Loopback' = [string]$Match.Loopback
            'Amazon' = [string]$Match.Connect
            'EID' = [string]$Match.Employee_ID
            'Department' = [string]$Match.Department
            'Note'  = [string]$Match.Notes
            'Domain' = 'Health'
            'Index' = $MatchIndex
        })
    }
    ForEach ($Match in $LifeFilter) {
        $MatchIndex = $LifeData.IndexOf($Match) + 2
        $MatchUser = $Match.Agent_Username
        Write-Host "Matched $MatchUser to $username in Life!"
        [void]$matchList.add([PSCustomObject]@{
            'Username' = [string]$MatchUser
            'Name' = [string]$Match.'first_name'
            'DNIS'= [string]$Match.'agent ani'
            'TempDNIS' = [string]$Match.Agent_Temp_DNIS
            'Extension' = [string]$Match.Agent_Extension
            'Campaign' = [string]$Match.Five9_Campaign_Group
            'Record' = [string]$Match.number1
            'Loopback' = [string]$Match.Loopback
            'Amazon' = [string]$Match.Connect
            'EID' = [string]$Match.Employee_ID
            'Department' = [string]$Match.Department
            'Note'  = [string]$Match.Notes
            'Domain' = 'Life'
            'Index' = $MatchIndex
        })
    }
    return $matchList
}


function Add-GoogleOffboardRecord {
    param (
        [Parameter(Mandatory=$true)][string]$token,
        [Parameter(Mandatory=$true)]$contactdata,
        [Parameter(Mandatory=$true)][string]$sheetID,
        [Parameter(Mandatory=$false)]$domain
    )

    $data = New-Object System.Collections.ArrayList($null)
    $Date = Get-Date -Format "MM.dd.yyyy"
    $data.add( @("Offboarded $Date", `
        $contactdata.Record, `
        $contactdata.Username, `
        $contactdata.Name, `
        $contactdata.DNIS, `
        $contactdata.TempDNIS, `
        $contactdata.Loopback, `
        $contactdata.Amazon, `
        $contactdata.Extension, `
        $contactdata.EID, `
        $contactdata.Campaign, `
        $contactdata.Department, `
        $contactdata.Note `
    ) ) | Out-Null
    
    try {
        Write-Host "Writing data to $domain Contact Sheet Offboarded Tab..."
        Write-SheetAppend $token $sheetID "Offboarded Agent Mapping" $data | Out-Null
        return 'TRUE'
    }
    catch {
        $_
        return 'FALSE'
    }
}

Function Remove-GoogleContactRecord ($index, $sectoken, $sheetid, $domain) {
    Write-Host "Deleting $index in $domain Contact Table..."
    try {
        Set-DeleteLine -token $sectoken -indexes $index -sheetName "Active Agent Mapping" -sheetID $sheetid  | Out-Null
        Start-Sleep -Milliseconds 500
        return 'TRUE'
    }
    catch {
        $_
        return 'FALSE'
    }
}

Function Remove-AllStationIDs ($sectoken, $sheetid, $username, $domain) {
    $StationID = Get-Sheet $sectoken $sheetid "Station IDs"
    $StationIDFilter = ($StationID | ForEach-Object {If ($_.'Five9 Username' -eq $username) {$_}})
    $errList = New-Object System.Collections.ArrayList($null) #This stores all errors
    ForEach ($Match in $StationIDFilter) {
        $IDIndex = $StationID.IndexOf($Match) + 2
        Write-Host "Deleting $IDIndex in $domain Station ID's..."
        try {
            Remove-StationID $sectoken $sheetid "Station IDs" $IDIndex | Out-Null
            Start-Sleep -Milliseconds 500
        }
        catch {
            $_
            [void]$errList.add([PSCustomObject]@{
                'Error' = $_.Exception.Response.StatusCode.value__
            })
        }
    }
    if (!$errList) {
        return 'TRUE'
    }
    else {
        return $errList
    }
}

Function Add-GoogleOffboardLog ($ContactRecord, $token, $offboardsheetid, $disabledtab) {
    $disableddata = New-Object System.Collections.ArrayList($null)
    $Date = Get-Date -Format "MM.dd.yyyy"
    $disableddata.add( @($Date, `
        $ContactRecord.Domain, `
        $ContactRecord.Username, `
        $ContactRecord.DNIS, `
        $ContactRecord.TempDNIS, `
        $ContactRecord.Campaign, `
        $ContactRecord.Record
    ) ) | Out-Null
    try{
        Write-SheetAppendDisabledUsers $token $offboardsheetid $disabledtab $disableddata | Out-Null
        return 'TRUE'
    }
    catch {
        $_
        return 'FALSE'
    }
}

#Checks for edge cases where there are duplicate usernames
Function Step-MultipleMatchesCheck ($token, $email, $records) {
    $MatchedRecordsUsernames = $records.Username
    $UniqueRecordsUsernames = ($MatchedRecordsUsernames | Sort-Object | Get-Unique)
    $DuplicateMatchEntries = New-Object System.Collections.ArrayList($null) #This stores multiple matches to return condition of function for reporting
    
    #For each unique username, check if it occurs more than once in the array and if it does send notice email and skip entry
    ForEach ($User in $UniqueRecordsUsernames) {
        $MultipleUserRecordFilter = ($records | ForEach-Object {If ($_.Username -eq $User) {$_}})
        if ($MultipleUserRecordFilter.Count -gt 1) {
            Write-Host 'Multiple matches for the same username were found, sending manual review notice email and continuing...'
            try {
                Send-MultiMatchGoogleEmail $token $email $MultipleUserRecordFilter
                [void]$DuplicateMatchEntries.add($User)
            }
            catch {
                $_
            }
        }
    }
    if ($DuplicateMatchEntries) {
        return 'TRUE'
    }
    else {
        return 'FALSE'
    }
}