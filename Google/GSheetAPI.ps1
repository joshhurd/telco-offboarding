#This function grabs the data from the provided SheetID and Tab
function Get-Sheet ($token, $sheetID, $sheetName) {
    $uri = "https://sheets.googleapis.com/v4/spreadsheets/$sheetID/values/$sheetName"+"?valueRenderOption=FORMATTED_VALUE"
    try {
        $ProgressPreference = 'SilentlyContinue'
        $result = Invoke-RestMethod -Method GET -Uri $uri -Headers @{"Authorization"="Bearer $token"} -UseBasicParsing
    }
    catch {
        Write-Host "There was an error..." -ForegroundColor Red
        Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
        Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
    }

    # Formatting the returned data
    $sheet = $result.values
    $Rows = $sheet.Count
    $Columns = $sheet[0].Count
    $HeaderRow = 0
    $Header = $sheet[0]
    if ($Rows -lt 2) {
        return $null #Return nothing if only header row
    }
    else {
        foreach ($Row in (($HeaderRow + 1)..($Rows-1))) { 
            $h = [Ordered]@{}
            foreach ($Column in 0..($Columns-1)) {
                if ($sheet[0][$Column].Length -gt 0) {
                    $Name = $Header[$Column]
                    $h.$Name = $Sheet[$Row][$Column]
                }
            }
            [PSCustomObject]$h
        }
    }
}

#Write data to line with specified index
function Write-Sheet ($token, $sheetID, $sheetName, $output, $index){
    $range = "A"+$index+":K"+$index
    $uri = "https://sheets.googleapis.com/v4/spreadsheets/$sheetID/values/$sheetName!$range"+"?valueInputOption=USER_ENTERED"
    $json = @{values=$output} | ConvertTo-Json
    try {
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Method PUT -Uri $uri -Body $json -ContentType "application/json" -Headers @{"Authorization"="Bearer $token"} -UseBasicParsing
    }
    catch{
        Write-Host "There was an error..." -ForegroundColor Red
        Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
        Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
    }
}

#This function will write to a sheet tab to the first empty row
function Write-SheetAppend ($token, $sheetID, $sheetName, $values) {
    $range = "A2:A"
    $uri = "https://sheets.googleapis.com/v4/spreadsheets/$sheetID/values/$sheetName!$range"+":append?valueInputOption=RAW&insertDataOption=INSERT_ROWS"
    $json = @{values=$values} | ConvertTo-Json -Depth 20
    try {
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Method POST -Uri $uri -Body $json -ContentType "application/json" -Headers @{"Authorization"="Bearer $token"} -UseBasicParsing
    }
    catch{
        $_
        throw {
            $_
        }
    }
}

#This function will write to a sheet tab to the first empty row
function Write-SheetAppendDisabledUsers ($token, $sheetID, $sheetName, $values) {
    $range = "A2:A"
    $uri = "https://sheets.googleapis.com/v4/spreadsheets/$sheetID/values/$sheetName!$range"+":append?valueInputOption=RAW&insertDataOption=INSERT_ROWS"
    $json = @{values=$values} | ConvertTo-Json -Depth 20
    try {
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Method POST -Uri $uri -Body $json -ContentType "application/json" -Headers @{"Authorization"="Bearer $token"} -UseBasicParsing
    }
    catch{
        $_
        throw {
            $_
        }
    }
}

#This function will delete lines at the indexes provided
function Set-DeleteLine {
    Param (
        [Parameter(Mandatory)][string]$token,
        [Parameter(Mandatory)]$indexes,
        [Parameter(Mandatory)][string]$sheetName,
        [Parameter(Mandatory)][string]$sheetID
    )
    
    $sheetToken = Get-SheetToken $sheetID $sheetName $token

    #Allow for bulk deletion by adding multiples to a list
    $requestArray = New-Object System.Collections.ArrayList($null)
    if ($indexes.Count -gt 1) {
        $orderedIndex = $indexes | Sort-Object #This makes it easier to group bulk deletion
        ForEach ($index in $orderedIndex) {
            $iteration = $orderedIndex.IndexOf($index)
            $startIndex = $index - $iteration - 1
            $endIndex = $startIndex + 1
            [void]$requestArray.add(@{"deleteDimension" = @{"range" = @{"sheetId" = $sheetToken; "dimension" = "ROWS"; `
                "startIndex" = $startIndex; "endIndex" = $endIndex}}})
        }
    }
    else {
        $startIndex = [int]$indexes[0] - 1
        $endIndex = [int]$indexes[0]
        [void]$requestArray.add(@{"deleteDimension" = @{"range" = @{"sheetId" = $sheetToken; "dimension" = "ROWS"; `
            "startIndex" = $startIndex; "endIndex" = $endIndex}}})
    }
    $uri = "https://sheets.googleapis.com/v4/spreadsheets/$sheetID"+":batchUpdate"
    $json = @{requests=$requestArray} | ConvertTo-Json -Depth 20
    try{
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Method POST -Uri $uri -Body $json -ContentType "application/json" -Headers @{"Authorization"="Bearer $token"}
    }
    catch{
        $_
        throw {
            $_
        }
    }
}

#This function returns the unique identifier for a page in a spreadsheet with multiple pages
function Get-SheetToken ($sheetID, $sheetName, $token) {
    $uri = "https://sheets.googleapis.com/v4/spreadsheets/$sheetID"
    $spreadSheetToken = Invoke-RestMethod -Method GET -Uri $uri -Headers @{"Authorization"="Bearer $token"}
    $sheetToken = ($spreadSheetToken.sheets.properties | Where-Object {$_.title -eq $sheetName}).sheetID
    return $sheetToken
}

function Set-ClearLine ($token, $sheetID, $sheetName, $index){
    $range = "A"+$index+":K"+$index
    $uri = "https://sheets.googleapis.com/v4/spreadsheets/$sheetID/values/$sheetName!$range"+":clear"
    try {
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Method POST -Uri $uri -Body "" -ContentType "application/json" -Headers @{"Authorization"="Bearer $token"} -UseBasicParsing
    }
    catch{
        $_
        throw {
            $_
        }
    }
}

function Remove-StationID ($accesstoken, $sheetID, $sheetName, $index){
    $range = "B"+$index
    $uri = "https://sheets.googleapis.com/v4/spreadsheets/$sheetID/values/$sheetName!$range"+":clear"
    try {
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Method POST -Uri $uri -Body "" -ContentType "application/json" -Headers @{"Authorization"="Bearer $accesstoken"} -UseBasicParsing
    }
    catch{
        $_
        throw {
            $_
        }
    }
}