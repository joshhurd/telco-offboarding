Function Get-Base64Url([string]$MsgIn) {
    $InputBytes =  [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($MsgIn))
    
    # "Url-Safe" base64 encodeing
    $InputBytes = $InputBytes.Replace('+', '-').Replace('/', '_').Replace("=", "")
    return $InputBytes
}

#Generate an email report
function Send-EmailReport ($token, $emailDestination, $ReportContent) {
    $to = "To: $emailDestination`n"
    $cc = "CC: `n"
    $reply = "Reply-To: telecom_ps@selectquote.com`n"
    $bcc = "Bcc: `n"
    $from = "From: telecom_ps@selectquote.com`n"
    $importance = "Importance: 0`n"
    $subject = "Subject: [REPORT] Offboarding Summary`n"
    $body = @'
Content-Type: multipart/alternative; boundary="000000000000d921e005de39ea79"

--000000000000d921e005de39ea79
Content-Type: text/plain; charset="UTF-8"

This is an automated report for offboarding:

'@ + $ReportContent + 
@'

*###This is an automated message and this inbox is not monitored###*
*------------------------------------------------------------------*
See https://bitbucket.org/joshhurd/telco-offboarding/src/main/ for 
more details on this project.
*------------------------------------------------------------------*

*Telecom| SelectQuote IT*

--000000000000d921e005de39ea79
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: quoted-printable

<div dir=3D"ltr">This is an automated report for offboarding:<div><br></div><=
div>
'@ + $ReportContent + 
@'
<br><div><br></div><div><i><font face=3D"monospace">###Thi=
s is an automated message and this inbox is not monitored###</font></i></di=
v><div><i><font face=3D"monospace">----------------------------------------=
--------------------------</font></i></div><div>See=C2=A0<a href=3D"https://=
bitbucket.org/joshhurd/telco-offboarding/src/main/">https://bitbucket.org/joshhurd/=
telco-offboarding/src/main/</a> for more details on this project.</div><div><i><fo=
nt face=3D"monospace">-----------------------------------------------------=
-------------</font></i><br></div><div><br></div><div><div dir=3D"ltr"><div=
    dir=3D"ltr"><div><font color=3D"#e69138" face=3D"trebuchet ms, sans-serif"=
><b>Telecom|=C2=A0SelectQuote IT</b></font></div></div></div></div><b style=
=3D"color:rgb(230,145,56);font-family:&quot;trebuchet ms&quot;,sans-serif">=
<img src=3D"https://ci6.googleusercontent.com/proxy/0s5A5S1oxPltkTT5Uw_nslU=
H_T4IPOytTwMyCpy6ovJdkPSW6thb6VGzFc0CsT5uPb9Blm9KrKrG3D926NZr7b-p1pnk0kwZma=
kLcdyAbtN2CrIaJg_tGk2OuSkUE5hZ62KM7sujmRB_Tw2pJ7-8eJE7bztFEit9JmkUG9lIdrThD=
3YTEbAxSin8JX25agQu5OZ-AL4YcPOT5x2raA=3Ds0-d-e1-ft#https://docs.google.com/=
uc?export=3Ddownload&amp;id=3D1VRrr4V_Ws9EuxFOM5_t4m6HYpCDDxPrd&amp;revid=
=3D0B2Alan3QKn4tbHU2WVc2eEp3MUdVeFNQZ0xzN3dFc2k3bWY0PQ" class=3D"gmail-CToW=
Ud"></b></div></div>

--000000000000d921e005de39ea79--
'@

    $MsgSW = ($to + $cc + $reply + $bcc + $from + $importance + $subject + $body)
    $EncodedEmail = Get-Base64Url $MsgSW

    $Content = @{ "raw" = $EncodedEmail; } | ConvertTo-Json
    try {
        Write-Host "Sending report email " -NoNewline -ForegroundColor Yellow
        Write-Host $to -NoNewline
        Write-Host "..." -ForegroundColor Yellow
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Uri "https://www.googleapis.com/gmail/v1/users/me/messages/send?access_token=$token" -Method POST -ErrorAction Stop -Body $Content -ContentType "Application/Json"
    }
    catch{
        $_
        throw {
            Write-Host "There was an error..." -ForegroundColor Red
            Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
            Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
        }
    }
}

#Generate missing data email
function Send-FailureEmail ($token, $emailDestination) {
    $to = "To: $emailDestination`n"
    $cc = "CC: `n"
    $reply = "Reply-To: telecom_ps@selectquote.com`n"
    $bcc = "Bcc: `n"
    $from = "From: telecom_ps@selectquote.com`n"
    $importance = "Importance: 0`n"
    $subject = "Subject: [NOTICE] Offboarding Request Declined`n"
    $body = @'
Content-Type: multipart/alternative; boundary="0000000000006299ce05e0f6958c"

--0000000000006299ce05e0f6958c
Content-Type: text/plain; charset="UTF-8"

Hello,

This is an automated notice that you submitted an offboarding request that
was *missing any actionable data*.

*Your entry was removed and no action was taken.* Please submit a request
that contains one of the following:

    - Employee ID
    - Employee Name
    - Username

You can use the following form to submit a new request:
https://forms.gle/23cWhNr8hMAyMviU8

*###This is an automated message and this inbox is not monitored###*
*------------------------------------------------------------------*
*Visit https://bitbucket.org/joshhurd/telco-offboarding/src/main/
<https://bitbucket.org/joshhurd/telco-offboarding/src/main/> for more
details on this project.*
*------------------------------------------------------------------*

*Telecom| SelectQuote IT*

--0000000000006299ce05e0f6958c
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: quoted-printable

<div dir=3D"ltr">Hello,<div><br></div><div>This is an automated notice that=
you submitted an offboarding request that was <b>missing any actionable da=
ta</b>.</div><br><b>Your entry was removed and no action was taken.</b> Ple=
ase submit a request that contains one of the following:<br><ul><li>Employe=
e ID</li><li>Employee Name</li><li>Username</li></ul><div>You can use the f=
ollowing form to submit a new request:=C2=A0<a href=3D"https://forms.gle/23=
cWhNr8hMAyMviU8">https://forms.gle/23cWhNr8hMAyMviU8</a></div><div><br></di=
v><div><i><font face=3D"monospace">###This is an automated message and this=
inbox is not monitored###</font></i></div><div><i><font face=3D"monospace"=
>------------------------------------------------------------------</font><=
/i></div><div><font face=3D"monospace"><i>Visit=C2=A0<a href=3D"https://bit=
bucket.org/joshhurd/telco-offboarding/src/main/">https://bitbucket.org/josh=
hurd/telco-offboarding/src/main/</a> for more details on this project.</i><=
/font></div><div><i><font face=3D"monospace">------------------------------=
------------------------------------</font></i><br></div><div><br></div><di=
v><div><div dir=3D"ltr"><div dir=3D"ltr"><div><font color=3D"#e69138" face=
=3D"trebuchet ms, sans-serif"><b>Telecom|=C2=A0SelectQuote IT</b></font></d=
iv><div><font color=3D"#e69138" face=3D"trebuchet ms, sans-serif"><b><img s=
rc=3D"https://ci6.googleusercontent.com/proxy/0s5A5S1oxPltkTT5Uw_nslUH_T4IP=
OytTwMyCpy6ovJdkPSW6thb6VGzFc0CsT5uPb9Blm9KrKrG3D926NZr7b-p1pnk0kwZmakLcdyA=
btN2CrIaJg_tGk2OuSkUE5hZ62KM7sujmRB_Tw2pJ7-8eJE7bztFEit9JmkUG9lIdrThD3YTEbA=
xSin8JX25agQu5OZ-AL4YcPOT5x2raA=3Ds0-d-e1-ft#https://docs.google.com/uc?exp=
ort=3Ddownload&amp;id=3D1VRrr4V_Ws9EuxFOM5_t4m6HYpCDDxPrd&amp;revid=3D0B2Al=
an3QKn4tbHU2WVc2eEp3MUdVeFNQZ0xzN3dFc2k3bWY0PQ" class=3D"gmail-CToWUd"></b>=
</font></div></div></div></div></div></div>

--0000000000006299ce05e0f6958c--
'@

    $MsgSW = ($to + $cc + $reply + $bcc + $from + $importance + $subject + $body)
    $EncodedEmail = Get-Base64Url $MsgSW

    $Content = @{ "raw" = $EncodedEmail; } | ConvertTo-Json
    try {
        Write-Host "Sending report email " -NoNewline -ForegroundColor Yellow
        Write-Host $to -NoNewline
        Write-Host "..." -ForegroundColor Yellow
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Uri "https://www.googleapis.com/gmail/v1/users/me/messages/send?access_token=$token" -Method POST -ErrorAction Stop -Body $Content -ContentType "Application/Json"
    }
    catch{
        $_
        throw {
            Write-Host "There was an error..." -ForegroundColor Red
            Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
            Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
        }
    }
}

#Generate missing data email
function Send-NoMatchesEmail ($token, $emailDestination, $userInfo) {
    $to = "To: $emailDestination`n"
    $cc = "CC: `n"
    $reply = "Reply-To: telecom_ps@selectquote.com`n"
    $bcc = "Bcc: `n"
    $from = "From: telecom_ps@selectquote.com`n"
    $importance = "Importance: 0`n"
    $subject = "Subject: [NOTICE] Offboarding Request Failed`n"
    $body = @'
Content-Type: multipart/alternative; boundary="0000000000006299ce05e0f6958c"

--0000000000006299ce05e0f6958c
Content-Type: text/plain; charset="UTF-8"

Hello,

This is an automated notice that you submitted an offboarding request that *didn't find any matches to offboard*.

*Your entry was removed and no action was taken.* No matches were found for the data provided:

'@ + ($userInfo | Out-String) +
@'


You can use the following form to submit a new request:
https://forms.gle/23cWhNr8hMAyMviU8

*###This is an automated message and this inbox is not monitored###*
*------------------------------------------------------------------*
*Visit https://bitbucket.org/joshhurd/telco-offboarding/src/main/
<https://bitbucket.org/joshhurd/telco-offboarding/src/main/> for more
details on this project.*
*------------------------------------------------------------------*

*Telecom| SelectQuote IT*

--0000000000006299ce05e0f6958c
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: quoted-printable

<div dir=3D"ltr">Hello,<div><br></div><div>This is an automated notice that=
 you submitted an offboarding request that <b>didn't find any matches to offboard</b>=
.</div><br><b>Your entry was removed and no action was taken.</b> No matches were found for the data provided:<br>
'@ + ($userInfo | Out-String) +
@'
<br><br><div>You can use the f=
ollowing form to submit a new request:=C2=A0<a href=3D"https://forms.gle/23=
cWhNr8hMAyMviU8">https://forms.gle/23cWhNr8hMAyMviU8</a></div><div><br></di=
v><div><i><font face=3D"monospace">###This is an automated message and this=
inbox is not monitored###</font></i></div><div><i><font face=3D"monospace"=
>------------------------------------------------------------------</font><=
/i></div><div><font face=3D"monospace"><i>Visit=C2=A0<a href=3D"https://bit=
bucket.org/joshhurd/telco-offboarding/src/main/">https://bitbucket.org/josh=
hurd/telco-offboarding/src/main/</a> for more details on this project.</i><=
/font></div><div><i><font face=3D"monospace">------------------------------=
------------------------------------</font></i><br></div><div><br></div><di=
v><div><div dir=3D"ltr"><div dir=3D"ltr"><div><font color=3D"#e69138" face=
=3D"trebuchet ms, sans-serif"><b>Telecom|=C2=A0SelectQuote IT</b></font></d=
iv><div><font color=3D"#e69138" face=3D"trebuchet ms, sans-serif"><b><img s=
rc=3D"https://ci6.googleusercontent.com/proxy/0s5A5S1oxPltkTT5Uw_nslUH_T4IP=
OytTwMyCpy6ovJdkPSW6thb6VGzFc0CsT5uPb9Blm9KrKrG3D926NZr7b-p1pnk0kwZmakLcdyA=
btN2CrIaJg_tGk2OuSkUE5hZ62KM7sujmRB_Tw2pJ7-8eJE7bztFEit9JmkUG9lIdrThD3YTEbA=
xSin8JX25agQu5OZ-AL4YcPOT5x2raA=3Ds0-d-e1-ft#https://docs.google.com/uc?exp=
ort=3Ddownload&amp;id=3D1VRrr4V_Ws9EuxFOM5_t4m6HYpCDDxPrd&amp;revid=3D0B2Al=
an3QKn4tbHU2WVc2eEp3MUdVeFNQZ0xzN3dFc2k3bWY0PQ" class=3D"gmail-CToWUd"></b>=
</font></div></div></div></div></div></div>

--0000000000006299ce05e0f6958c--
'@

    $MsgSW = ($to + $cc + $reply + $bcc + $from + $importance + $subject + $body)
    $EncodedEmail = Get-Base64Url $MsgSW

    $Content = @{ "raw" = $EncodedEmail; } | ConvertTo-Json
    try {
        Write-Host "Sending report email " -NoNewline -ForegroundColor Yellow
        Write-Host $to -NoNewline
        Write-Host "..." -ForegroundColor Yellow
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Uri "https://www.googleapis.com/gmail/v1/users/me/messages/send?access_token=$token" -Method POST -ErrorAction Stop -Body $Content -ContentType "Application/Json"
    }
    catch{
        $_
        throw {
            Write-Host "There was an error..." -ForegroundColor Red
            Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
            Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
        }
    }
}

#Generate multiple matches email
function Send-MultiMatchGoogleEmail ($token, $emailDestination, $records) {
    $to = "To: $emailDestination`n"
    $cc = "CC: `n"
    $reply = "Reply-To: telecom_ps@selectquote.com`n"
    $bcc = "Bcc: `n"
    $from = "From: telecom_ps@selectquote.com`n"
    $importance = "Importance: 0`n"
    $subject = "Subject: [NOTICE] Offboarding Request Declined`n"

    $emaildatalist = New-Object System.Collections.ArrayList($null)
    ForEach ($record in $records) {
        $emaildata = @'
<b>Domain: </b>
'@ + $record.Domain + '<br>' +
@'
<b>Row: </b>
'@ + $record.Index + '<br>'
        [void]$emaildatalist.add($emaildata)
    }
    $username = $records.Username[0]
    $emailbodydata = ($emaildatalist | Out-String)

    $body = @'
Content-Type: multipart/alternative; boundary="00000000000056557705e0f8a879"

--00000000000056557705e0f8a879
Content-Type: text/plain; charset="UTF-8"

Hello,

This is an automated notice that the following *offboarding request failed*. Please review these records manually:

'@ + $username +
@'
 returned multiple matches:
'@ + $emailbodydata +
@'

*###This is an automated message and this inbox is not monitored###*
*------------------------------------------------------------------*
*Visit https://bitbucket.org/joshhurd/telco-offboarding/src/main/
<https://bitbucket.org/joshhurd/telco-offboarding/src/main/> for more
details on this project.*
*------------------------------------------------------------------*

*Telecom| SelectQuote IT*

--00000000000056557705e0f8a879
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: quoted-printable

<div dir=3D"ltr">Hello,<div><br></div><div>This is an automated notice that =
the following <b>offboarding request failed</b>. Please review these records manually:</div><br>
'@ + $username +
@' 
 return=
ed multiple matches:<div>
'@ + $emailbodydata +
@'
<br><div><=
br></div><div><i><font face=3D"monospace">###This is an automated message a=
nd this inbox is not monitored###</font></i></div><div><i><font face=3D"mon=
ospace">------------------------------------------------------------------<=
/font></i></div><div><font face=3D"monospace"><i>Visit=C2=A0<a href=3D"http=
s://bitbucket.org/joshhurd/telco-offboarding/src/main/" target=3D"_blank">h=
ttps://bitbucket.org/joshhurd/telco-offboarding/src/main/</a>=C2=A0for more=
details on this project.</i></font></div><div><i><font face=3D"monospace">=
------------------------------------------------------------------</font></=
i><br></div><div><br></div><div><div><div dir=3D"ltr"><div dir=3D"ltr"><div=
><font color=3D"#e69138" face=3D"trebuchet ms, sans-serif"><b>Telecom|=C2=
=A0SelectQuote IT</b></font></div><div><font color=3D"#e69138" face=3D"treb=
uchet ms, sans-serif"><b><img src=3D"https://ci6.googleusercontent.com/prox=
y/0s5A5S1oxPltkTT5Uw_nslUH_T4IPOytTwMyCpy6ovJdkPSW6thb6VGzFc0CsT5uPb9Blm9Kr=
KrG3D926NZr7b-p1pnk0kwZmakLcdyAbtN2CrIaJg_tGk2OuSkUE5hZ62KM7sujmRB_Tw2pJ7-8=
eJE7bztFEit9JmkUG9lIdrThD3YTEbAxSin8JX25agQu5OZ-AL4YcPOT5x2raA=3Ds0-d-e1-ft=
#https://docs.google.com/uc?export=3Ddownload&amp;id=3D1VRrr4V_Ws9EuxFOM5_t=
4m6HYpCDDxPrd&amp;revid=3D0B2Alan3QKn4tbHU2WVc2eEp3MUdVeFNQZ0xzN3dFc2k3bWY0=
PQ" class=3D"gmail-CToWUd"></b></font></div></div></div></div></div></div><=
/div>

--00000000000056557705e0f8a879--
'@

    $MsgSW = ($to + $cc + $reply + $bcc + $from + $importance + $subject + $body)
    $EncodedEmail = Get-Base64Url $MsgSW

    $Content = @{ "raw" = $EncodedEmail; } | ConvertTo-Json
    try {
        Write-Host "Sending report email " -NoNewline -ForegroundColor Yellow
        Write-Host $to -NoNewline
        Write-Host "..." -ForegroundColor Yellow
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Uri "https://www.googleapis.com/gmail/v1/users/me/messages/send?access_token=$token" -Method POST -ErrorAction Stop -Body $Content -ContentType "Application/Json"
    }
    catch{
        $_
        throw {
            Write-Host "There was an error..." -ForegroundColor Red
            Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
            Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
        }
    }
}

#Generate multiple matches email
function Send-MultiMatchFive9Email ($token, $emailDestination, $records) {
    $to = "To: $emailDestination`n"
    $cc = "CC: `n"
    $reply = "Reply-To: telecom_ps@selectquote.com`n"
    $bcc = "Bcc: `n"
    $from = "From: telecom_ps@selectquote.com`n"
    $importance = "Importance: 0`n"
    $subject = "Subject: [NOTICE] Offboarding Request Declined`n"

    $emaildatalist = New-Object System.Collections.ArrayList($null)
    ForEach ($record in $records) {
        $emaildata = @'
<b>Domain: </b>
'@ + $record.Domain + '<br>' +
@'
<b>Username: </b>
'@ + $record.Username + '<br>'
        [void]$emaildatalist.add($emaildata)
    }
    $name = $records.Name[0]
    $emailbodydata = ($emaildatalist | Out-String)

    $body = @'
Content-Type: multipart/alternative; boundary="00000000000056557705e0f8a879"

--00000000000056557705e0f8a879
Content-Type: text/plain; charset="UTF-8"

Hello,

This is an automated notice that the following *offboarding request failed*. There were too many matches, please review these accounts and enter them individually:

'@ + $name +
@'
 returned multiple matches:
'@ + $emailbodydata +
@'

*###This is an automated message and this inbox is not monitored###*
*------------------------------------------------------------------*
*Visit https://bitbucket.org/joshhurd/telco-offboarding/src/main/
<https://bitbucket.org/joshhurd/telco-offboarding/src/main/> for more
details on this project.*
*------------------------------------------------------------------*

*Telecom| SelectQuote IT*

--00000000000056557705e0f8a879
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: quoted-printable

<div dir=3D"ltr">Hello,<div><br></div><div>This is an automated notice that =
the following <b>offboarding request failed</b>. There were too many matches, please review these accounts and enter them individually:</div><br>
'@ + $name +
@' 
 return=
ed multiple matches:<div>
'@ + $emailbodydata +
@'
<br><div><=
br></div><div><i><font face=3D"monospace">###This is an automated message a=
nd this inbox is not monitored###</font></i></div><div><i><font face=3D"mon=
ospace">------------------------------------------------------------------<=
/font></i></div><div><font face=3D"monospace"><i>Visit=C2=A0<a href=3D"http=
s://bitbucket.org/joshhurd/telco-offboarding/src/main/" target=3D"_blank">h=
ttps://bitbucket.org/joshhurd/telco-offboarding/src/main/</a>=C2=A0for more=
details on this project.</i></font></div><div><i><font face=3D"monospace">=
------------------------------------------------------------------</font></=
i><br></div><div><br></div><div><div><div dir=3D"ltr"><div dir=3D"ltr"><div=
><font color=3D"#e69138" face=3D"trebuchet ms, sans-serif"><b>Telecom|=C2=
=A0SelectQuote IT</b></font></div><div><font color=3D"#e69138" face=3D"treb=
uchet ms, sans-serif"><b><img src=3D"https://ci6.googleusercontent.com/prox=
y/0s5A5S1oxPltkTT5Uw_nslUH_T4IPOytTwMyCpy6ovJdkPSW6thb6VGzFc0CsT5uPb9Blm9Kr=
KrG3D926NZr7b-p1pnk0kwZmakLcdyAbtN2CrIaJg_tGk2OuSkUE5hZ62KM7sujmRB_Tw2pJ7-8=
eJE7bztFEit9JmkUG9lIdrThD3YTEbAxSin8JX25agQu5OZ-AL4YcPOT5x2raA=3Ds0-d-e1-ft=
#https://docs.google.com/uc?export=3Ddownload&amp;id=3D1VRrr4V_Ws9EuxFOM5_t=
4m6HYpCDDxPrd&amp;revid=3D0B2Alan3QKn4tbHU2WVc2eEp3MUdVeFNQZ0xzN3dFc2k3bWY0=
PQ" class=3D"gmail-CToWUd"></b></font></div></div></div></div></div></div><=
/div>

--00000000000056557705e0f8a879--
'@

    $MsgSW = ($to + $cc + $reply + $bcc + $from + $importance + $subject + $body)
    $EncodedEmail = Get-Base64Url $MsgSW

    $Content = @{ "raw" = $EncodedEmail; } | ConvertTo-Json
    try {
        Write-Host "Sending report email " -NoNewline -ForegroundColor Yellow
        Write-Host $to -NoNewline
        Write-Host "..." -ForegroundColor Yellow
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Uri "https://www.googleapis.com/gmail/v1/users/me/messages/send?access_token=$token" -Method POST -ErrorAction Stop -Body $Content -ContentType "Application/Json"
    }
    catch{
        $_
        throw {
            Write-Host "There was an error..." -ForegroundColor Red
            Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
            Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
        }
    }
}