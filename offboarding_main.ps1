#!/usr/bin/env powershell
#=====================================================================
# Telecom Offboarding Tool
# Written By: Joshua Hurd & James Porteous
# Contact: joshua.hurd@selectquote.com, james.porteous@selectquote.com
#           alan.noble@selectquote.com, telcoteam@selectquote.com
# Division: Telecom
# External Modules: 
#   https://github.com/sqone2/PSFive9Admin
#   https://github.com/umn-microsoft-automation/UMN-Google
#=====================================================================

#Pull variables from config.txt
Get-Content "$PSScriptRoot\config.txt" | ForEach-Object {
    $var = $_.Split('=')
    New-Variable -Name $var[0] -Value $var[1]
}

#Import other files
Import-Module "$PSScriptRoot\Google\OAuth.ps1"
Import-Module "$PSScriptRoot\Google\GSheetAPI.ps1"
Import-Module "$PSScriptRoot\Google\GSheetFuncs.ps1"
Import-Module "$PSScriptRoot\Google\GMailAPI.ps1"
Import-Module "$PSScriptRoot\Five9\Five9API.ps1"
Import-Module "$PSScriptRoot\Five9\Five9Authorization.ps1"
Import-Module "$PSScriptRoot\Five9\Five9Funcs.ps1"

#Connect to Google API and grab sheet data
Write-Host "Grabbing Google Offboarding Data..." -ForegroundColor Yellow
$GoogleAPICreds = Get-GoogleToken $PSScriptRoot
$SheetData = Get-Sheet $GoogleAPICreds $Offboarding_SheetID $Offboarding_SheetTab

try {
    #Create a logfile in Logging
    $date = Get-Date -Format "MM-dd-yyyy-hhmm"
    Start-Transcript -Path "$PSScriptRoot\Logging\OffboardTask-$date.log" -Append

    if (!$SheetData) {
        Write-Host "No entries in Google Sheet found, ending task." -ForegroundColor Yellow
        Exit
    }

    #Get secure credentials for Five9
    $SeniorCreds = Get-Five9Credentials "Senior" $PSScriptRoot
    $UHCCreds = Get-Five9Credentials "UHC" $PSScriptRoot
    $CCACreds = Get-Five9Credentials "CCA" $PSScriptRoot
    $SQAHCreds = Get-Five9Credentials "SQAH" $PSScriptRoot
    $HealthCreds = Get-Five9Credentials "Health" $PSScriptRoot
    $LifeCreds = Get-Five9Credentials "Life" $PSScriptRoot

    $emptyLines = New-Object System.Collections.ArrayList($null) #This is used to cleanup empty lines at the end
    $SuperRecord = New-Object System.Collections.ArrayList($null) #This is used to track all of the data for the matched user

    #Bulk information processing to set up further tasks
    if ($SheetData){
        ForEach ($Line in $SheetData) {
            $counter++
            if ($counter -gt 25) {
                Continue #This limits the number of requests processed at once to 25
            }
            #Define data from task sheet
            $Timestamp = $Line.Timestamp
            $EmployeeName = $Line.'Employee Name'
            $EmployeeID = $Line.'Employee ID'
            $Username = $Line.Username
            $Domain = $Line.Domain
            $Requester = $Line.'Email Address'
            #Fix EID if a number is provided and not 6 digits
            if ($EmployeeID) {
                if ($EmployeeID.count -ne 6) {
                    $EmployeeID = $EmployeeID.PadLeft(6, '0')
                }
            }
            if ($SheetData.Count -gt 1){
                $index = $SheetData.IndexOf($Line) + 2
            }
            else{
                $index = 2
            }
            #Check if the line is blank
            if (!$Timestamp -and !$EmployeeName -and !$EmployeeID -and !$Username -and !$Domain -and !$Requester) {
                [void]$emptyLines.add($index)
                Continue
            }
            #Check if an email address was provided
            if (!$Requester) {
                $MissingRowData = New-Object System.Collections.ArrayList($null)
                $MissingRowData.add( @($Timestamp, $EmployeeID, $EmployeeName, $Username, $Domain, '[ERROR] No address was provided') )
                Write-Sheet $GoogleAPICreds $Offboarding_SheetID 'Pending' $MissingRowData $index
                Continue
            }
            #Check if useful data was supplied
            if ((!$EmployeeID -and !$Username -and !$EmployeeName) -and $Requester) {
                Write-Host "Missing any actionable data... alerting requester and removing entry."
                try {
                    Send-FailureEmail $GoogleAPICreds $Requester | Out-Null
                    [void]$emptyLines.add($index)
                }
                catch {
                    $_
                }
                Continue
            }

            ###################################################################
            #This section begins the record matching steps. The objective is to
            #find record matches and sort them into lists with all needed data.
            #Due to the possibility of human error from the source tables,
            #there is a lot of double checking that takes place here.
            ###################################################################
            #If searching all records
            if ($EmployeeID -and !$Username -and !$Domain) {
                #Fetch contact sheet data
                if (!$SeniorActiveData) {
                    Write-Host "Fetching data from Google Contact Tables for Senior..."
                    $SeniorActiveData = Get-Sheet $GoogleAPICreds $Senior_SheetID "Active Agent Mapping"
                }
                if (!$UHCActiveData) {
                    Write-Host "Fetching data from Google Contact Tables for UHC..."
                    $UHCActiveData = Get-Sheet $GoogleAPICreds $UHC_SheetID "Active Agent Mapping"
                }
                if (!$CCAActiveData) {
                    Write-Host "Fetching data from Google Contact Tables for CCA..."
                    $CCAActiveData = Get-Sheet $GoogleAPICreds $CCA_SheetID "Active Agent Mapping"
                }
                if (!$SQAHActiveData) {
                    Write-Host "Fetching data from Google Contact Tables for SQAH..."
                    $SQAHActiveData = Get-Sheet $GoogleAPICreds $SQAH_SheetID "Active Agent Mapping"
                }
                if (!$HealthActiveData) {
                    Write-Host "Fetching data from Google Contact Tables for Health..."
                    $HealthActiveData = Get-Sheet $GoogleAPICreds $Health_SheetID "Active Agent Mapping"
                }
                if (!$LifeActiveData) {
                    Write-Host "Fetching data from Google Contact Tables for Life..."
                    $LifeActiveData = Get-Sheet $GoogleAPICreds $Life_SheetID "Active Agent Mapping"
                }

                $MatchedRecords = Get-EmployeeIDMatches `
                    -token $GoogleAPICreds `
                    -EID $EmployeeID `
                    -SeniorData $SeniorActiveData `
                    -UHCData $UHCActiveData `
                    -CCAData $CCAActiveData `
                    -SQAHData $SQAHActiveData `
                    -HealthData $HealthActiveData `
                    -LifeData $LifeActiveData

                #Check for edge cases where there are duplicate usernames
                $CheckforMultiple = Step-MultipleMatchesCheck $GoogleAPICreds $Requester $MatchedRecords
                if ($CheckforMultiple -eq 'TRUE') {
                    [void]$emptyLines.add($index)
                    Continue
                }
            }
            #If searching for a single domain
            elseif ($EmployeeID -and $Domain -and !$Username) {
                if ($Domain -eq 'Senior') {
                    if (!$SeniorActiveData) {
                        Write-Host "Fetching data from Google Contact Tables for Senior..."
                        $SeniorActiveData = Get-Sheet $GoogleAPICreds $Senior_SheetID "Active Agent Mapping"
                    }
                    $MatchedRecords = Get-EmployeeIDMatches `
                    -token $GoogleAPICreds `
                    -EID $EmployeeID `
                    -SeniorData $SeniorActiveData
                }
                if ($Domain -eq 'UHC') {
                    if (!$UHCActiveData) {
                        Write-Host "Fetching data from Google Contact Tables for UHC..."
                        $UHCActiveData = Get-Sheet $GoogleAPICreds $UHC_SheetID "Active Agent Mapping"
                    }
                    $MatchedRecords = Get-EmployeeIDMatches `
                    -token $GoogleAPICreds `
                    -EID $EmployeeID `
                    -UHCData $UHCActiveData
                }
                if ($Domain -eq 'CCA') {
                    if (!$CCAActiveData) {
                        Write-Host "Fetching data from Google Contact Tables for CCA..."
                        $CCAActiveData = Get-Sheet $GoogleAPICreds $CCA_SheetID "Active Agent Mapping"
                    }
                    $MatchedRecords = Get-EmployeeIDMatches `
                    -token $GoogleAPICreds `
                    -EID $EmployeeID `
                    -CCAData $CCAActiveData
                }
                if ($Domain -eq 'SQAH') {
                    if (!$SQAHActiveData) {
                        Write-Host "Fetching data from Google Contact Tables for SQAH..."
                        $SQAHActiveData = Get-Sheet $GoogleAPICreds $SQAH_SheetID "Active Agent Mapping"
                    }
                    $MatchedRecords = Get-EmployeeIDMatches `
                    -token $GoogleAPICreds `
                    -EID $EmployeeID `
                    -SQAHData $SQAHActiveData
                }
                if ($Domain -eq 'Health') {
                    if (!$HealthActiveData) {
                        Write-Host "Fetching data from Google Contact Tables for Health..."
                        $HealthActiveData = Get-Sheet $GoogleAPICreds $Health_SheetID "Active Agent Mapping"
                    }
                    $MatchedRecords = Get-EmployeeIDMatches `
                    -token $GoogleAPICreds `
                    -EID $EmployeeID `
                    -HealthData $HealthActiveData
                }
                if ($Domain -eq 'Life') {
                    if (!$LifeActiveData) {
                        Write-Host "Fetching data from Google Contact Tables for Life..."
                        $LifeActiveData = Get-Sheet $GoogleAPICreds $Life_SheetID "Active Agent Mapping"
                    }
                    $MatchedRecords = Get-EmployeeIDMatches `
                    -token $GoogleAPICreds `
                    -EID $EmployeeID `
                    -LifeData $LifeActiveData
                }

                #Check for edge cases where there are duplicate usernames
                $CheckforMultiple = Step-MultipleMatchesCheck $GoogleAPICreds $Requester $MatchedRecords
                if ($CheckforMultiple -eq 'TRUE') {
                    [void]$emptyLines.add($index)
                    Continue
                }
            }
            #If searching for a single username
            elseif ($Username) {
                #Fetch contact sheet data
                if (!$SeniorActiveData) {
                    Write-Host "Fetching data from Google Contact Tables for Senior..."
                    $SeniorActiveData = Get-Sheet $GoogleAPICreds $Senior_SheetID "Active Agent Mapping"
                }
                if (!$UHCActiveData) {
                    Write-Host "Fetching data from Google Contact Tables for UHC..."
                    $UHCActiveData = Get-Sheet $GoogleAPICreds $UHC_SheetID "Active Agent Mapping"
                }
                if (!$CCAActiveData) {
                    Write-Host "Fetching data from Google Contact Tables for CCA..."
                    $CCAActiveData = Get-Sheet $GoogleAPICreds $CCA_SheetID "Active Agent Mapping"
                }
                if (!$SQAHActiveData) {
                    Write-Host "Fetching data from Google Contact Tables for SQAH..."
                    $SQAHActiveData = Get-Sheet $GoogleAPICreds $SQAH_SheetID "Active Agent Mapping"
                }
                if (!$HealthActiveData) {
                    Write-Host "Fetching data from Google Contact Tables for Health..."
                    $HealthActiveData = Get-Sheet $GoogleAPICreds $Health_SheetID "Active Agent Mapping"
                }
                if (!$LifeActiveData) {
                    Write-Host "Fetching data from Google Contact Tables for Life..."
                    $LifeActiveData = Get-Sheet $GoogleAPICreds $Life_SheetID "Active Agent Mapping"
                }

                $MatchedRecords = Get-UsernameMatches `
                    -token $GoogleAPICreds `
                    -username $Username `
                    -SeniorData $SeniorActiveData `
                    -UHCData $UHCActiveData `
                    -CCAData $CCAActiveData `
                    -SQAHData $SQAHActiveData `
                    -HealthData $HealthActiveData `
                    -LifeData $LifeActiveData

                #Check for edge cases where there are duplicate usernames
                $CheckforMultiple = Step-MultipleMatchesCheck $GoogleAPICreds $Requester $MatchedRecords
                if ($CheckforMultiple -eq 'TRUE') {
                    [void]$emptyLines.add($index)
                    Continue
                }
            }
            #Add requester to any matched records in Google Sheets
            ForEach ($Match in $MatchedRecords) {
                [void]$SuperRecord.add($Match)
                $SuperRecord | Where-Object {$_.Username -eq $Match.Username} |
                    Add-Member -MemberType NoteProperty -Name 'Offboard Requester' -Value $Requester
                $SuperRecord | Where-Object {$_.Username -eq $Match.Username} |
                    Add-Member -MemberType NoteProperty -Name 'MatchType' -Value 'Google'
            }
            #Search Five9 domains if no contact records are found
            if (!$MatchedRecords) {
                #Search by employee name
                if ($EmployeeName -and !$Domain -and !$Username) {
                    Write-Host "No contact records found for $EmployeeName... checking user reports from Five9..." -ForegroundColor Yellow
                    #Connect to each domain and get report
                    if (!$SeniorUserReport) {
                        Write-Host "Fetching user report from Senior..."
                        Connect-Five9AdminWebService $SeniorCreds
                        $SeniorUserReport = Get-Five9UserReport
                    }
                    if (!$UHCUserReport) {
                        Write-Host "Fetching user report from UHC..."
                        Connect-Five9AdminWebService $UHCCreds
                        $UHCUserReport = Get-Five9UserReport
                    }
                    if (!$CCAUserReport) {
                        Write-Host "Fetching user report from CCA..."
                        Connect-Five9AdminWebService $CCACreds
                        $CCAUserReport = Get-Five9UserReport
                    }
                    if (!$SQAHUserReport) {
                        Write-Host "Fetching user report from SQAH..."
                        Connect-Five9AdminWebService $SQAHCreds
                        $SQAHUserReport = Get-Five9UserReport
                    }
                    if (!$HealthUserReport) {
                        Write-Host "Fetching user report from Health..."
                        Connect-Five9AdminWebService $HealthCreds
                        $HealthUserReport = Get-Five9UserReport
                    }
                    if (!$LifeUserReport) {
                        Write-Host "Fetching user report from Life..."
                        Connect-Five9AdminWebService $LifeCreds
                        $LifeUserReport = Get-Five9UserReport
                    }

                    $MatchedUsers = Get-Five9Username `
                        -name $EmployeeName `
                        -SeniorReport $SeniorUserReport `
                        -UHCReport $UHCUserReport `
                        -CCAReport $CCAUserReport `
                        -SQAHReport $SQAHUserReport `
                        -HealthReport $HealthUserReport `
                        -LifeReport $LifeUserReport

                    #Check to make sure only one account was found -- This is to set a high confidence level and prevent offboarding of wrong users
                    if ($MatchedUsers.Count -gt 1) {
                        Write-Host 'Multiple matches for the same name were found, sending manual review notice email and continuing...'
                        try {
                            Send-MultiMatchFive9Email $GoogleAPICreds $Requester $MatchedUsers
                            [void]$emptyLines.add($index)
                            Continue
                        }
                        catch {
                            $_
                        }
                    }
                }
                #Search if domain provided
                elseif ($EmployeeName -and $Domain -and !$Username) {
                    Write-Host "No contact records found for $EmployeeName in $Domain... checking user reports from Five9..." -ForegroundColor Yellow
                    if ($Domain -eq 'Senior') {
                        if (!$SeniorUserReport) {
                            Write-Host "Fetching user report from Senior..."
                            Connect-Five9AdminWebService $SeniorCreds
                            $SeniorUserReport = Get-Five9UserReport
                        }
                        $MatchedUsers = Get-Five9Username `
                            -name $EmployeeName `
                            -SeniorReport $SeniorUserReport
                    }
                    if ($Domain -eq 'UHC') {
                        if (!$UHCUserReport) {
                            Write-Host "Fetching user report from UHC..."
                            Connect-Five9AdminWebService $UHCCreds
                            $UHCUserReport = Get-Five9UserReport
                        }
                        $MatchedUsers = Get-Five9Username `
                            -name $EmployeeName `
                            -UHCReport $UHCUserReport
                    }
                    if ($Domain -eq 'CCA') {
                        if (!$CCAUserReport) {
                            Write-Host "Fetching user report from CCA..."
                            Connect-Five9AdminWebService $CCACreds
                            $CCAUserReport = Get-Five9UserReport
                        }
                        $MatchedUsers = Get-Five9Username `
                            -name $EmployeeName `
                            -CCAReport $CCAUserReport
                    }
                    if ($Domain -eq 'SQAH') {
                        if (!$SQAHUserReport) {
                            Write-Host "Fetching user report from SQAH..."
                            Connect-Five9AdminWebService $SQAHCreds
                            $SQAHUserReport = Get-Five9UserReport
                        }
                        $MatchedUsers = Get-Five9Username `
                            -name $EmployeeName `
                            -SQAHReport $SQAHUserReport
                    }
                    if ($Domain -eq 'Health') {
                        if (!$HealthUserReport) {
                            Write-Host "Fetching user report from Health..."
                            Connect-Five9AdminWebService $HealthCreds
                            $HealthUserReport = Get-Five9UserReport
                        }
                        $MatchedUsers = Get-Five9Username `
                            -name $EmployeeName `
                            -HealthReport $HealthUserReport
                    }
                    if ($Domain -eq 'Life') {
                        if (!$LifeUserReport) {
                            Write-Host "Fetching user report from Life..."
                            Connect-Five9AdminWebService $LifeCreds
                            $LifeUserReport = Get-Five9UserReport
                        }
                        $MatchedUsers = Get-Five9Username `
                            -name $EmployeeName `
                            -LifeReport $LifeUserReport
                    }

                    #Check to make sure only one account was found -- This is to set a high confidence level and prevent offboarding of wrong users
                    if ($MatchedUsers.Count -gt 1) {
                        Write-Host 'Multiple matches for the same username were found, sending manual review notice email and continuing...'
                        try {
                            Send-MultiMatchFive9Email $GoogleAPICreds $Requester $MatchedUsers
                            [void]$emptyLines.add($index)
                            Continue
                        }
                        catch {
                            $_
                        }
                    }
                }
                #Search if username provided
                elseif ($Username) {
                    Write-Host "No contact records found for $Username... checking user reports from Five9..." -ForegroundColor Yellow
                    #Connect to each domain and get report
                    if (!$SeniorUserReport) {
                        Write-Host "Fetching user report from Senior..."
                        Connect-Five9AdminWebService $SeniorCreds
                        $SeniorUserReport = Get-Five9UserReport
                    }
                    if (!$UHCUserReport) {
                        Write-Host "Fetching user report from UHC..."
                        Connect-Five9AdminWebService $UHCCreds
                        $UHCUserReport = Get-Five9UserReport
                    }
                    if (!$CCAUserReport) {
                        Write-Host "Fetching user report from CCA..."
                        Connect-Five9AdminWebService $CCACreds
                        $CCAUserReport = Get-Five9UserReport
                    }
                    if (!$SQAHUserReport) {
                        Write-Host "Fetching user report from SQAH..."
                        Connect-Five9AdminWebService $SQAHCreds
                        $SQAHUserReport = Get-Five9UserReport
                    }
                    if (!$HealthUserReport) {
                        Write-Host "Fetching user report from Health..."
                        Connect-Five9AdminWebService $HealthCreds
                        $HealthUserReport = Get-Five9UserReport
                    }
                    if (!$LifeUserReport) {
                        Write-Host "Fetching user report from Life..."
                        Connect-Five9AdminWebService $LifeCreds
                        $LifeUserReport = Get-Five9UserReport
                    }

                    $MatchedUsers = Get-Five9UserDomain `
                        -username $Username `
                        -SeniorReport $SeniorUserReport `
                        -UHCReport $UHCUserReport `
                        -CCAReport $CCAUserReport `
                        -SQAHReport $SQAHUserReport `
                        -HealthReport $HealthUserReport `
                        -LifeReport $LifeUserReport
                }
                #Add requester to any matched users in Five9
                ForEach ($Match in $MatchedUsers) {
                    [void]$SuperRecord.add($Match)
                    $SuperRecord | Where-Object {$_.Username -eq $Match.Username} |
                        Add-Member -MemberType NoteProperty -Name 'Offboard Requester' -Value $Requester
                    $SuperRecord | Where-Object {$_.Username -eq $Match.Username} |
                        Add-Member -MemberType NoteProperty -Name 'MatchType' -Value 'Five9'
                }
            }
            #Clear line in offboarding sheet
            if ($MatchedRecords -or $MatchedUsers) {
                [void]$emptyLines.add($index)
            }
            #Send report email if no matches were found
            if (!$MatchedRecords -and !$MatchedUsers) {
                $RequestData = @($EmployeeID, $EmployeeName, $Username, $Domain)
                Send-NoMatchesEmail $GoogleAPICreds $Requester $RequestData
                [void]$emptyLines.add($index)
                Continue
            }
        }

        #Create filters for each domain
        if ($SuperRecord) {
            $SeniorFilter = ($SuperRecord | ForEach-Object {If ($_.Domain -eq 'Senior') {$_}})
            $UHCFilter = ($SuperRecord | ForEach-Object {If ($_.Domain -eq 'UHC') {$_}})
            $CCAFilter = ($SuperRecord | ForEach-Object {If ($_.Domain -eq 'CCA') {$_}})
            $SQAHFilter = ($SuperRecord | ForEach-Object {If ($_.Domain -eq 'SQAH') {$_}})
            $HealthFilter = ($SuperRecord | ForEach-Object {If ($_.Domain -eq 'Health') {$_}})
            $LifeFilter = ($SuperRecord | ForEach-Object {If ($_.Domain -eq 'Life') {$_}})
        }

        ###################################################################
        #This section begins the Five9 steps. These filters are a bit wordy
        #for reporting purposes. It's important to track many of these
        #steps separately for accurate reports.
        ###################################################################

        #SENIOR-SENIOR-SENIOR-SENIOR-SENIOR-SENIOR-SENIOR-SENIOR-SENIOR-SENIOR-SENIOR-SENIOR-SENIOR-SENIOR#
        ###################################################################################################
        if ($SeniorFilter) {
            Connect-Five9AdminWebService -Credential $SeniorCreds #Connection to Five9 Senior (required before next actions)

            $SeniorDNISList = Step-DNISProcessing $SeniorFilter
            $SeniorRecordIndex = New-Object System.Collections.ArrayList($null) #This is used to track indexes for each record

            #Grab Five9 Senior data
            if ($SeniorDNISList) {
                Write-Host "Fetching Senior DNIS report..." -ForegroundColor Yellow
                $SeniorDNISReport = Get-Five9DNISReport
            }
            if (!$SeniorUserReport) {
                Write-Host "Fetching Senior User report..." -ForegroundColor Yellow
                $SeniorUserReport = Get-Five9UserReport
            }

            #Move DNIS to deactivated campaign
            if ($SeniorDNISList) {
                $SeniorDNISActionList = Get-Five9Campaigns $SeniorDNISList $SeniorDNISReport
                if ($SeniorDNISActionList -ne 'FALSE') {
                    try {
                        Remove-Five9DNIS $SeniorDNISActionList
                        $SuperRecord | Where-Object {($_.Domain -eq 'Senior')} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'SUCCESS'
                    }
                    catch {
                        $_
                        $SuperRecord | Where-Object {$_.Domain -eq 'Senior'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'FAILURE'
                    }
                }
                else {
                    $SuperRecord | Where-Object {$_.Domain -eq 'Senior'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'ALREADY IN DESTINATION CAMPAIGN'
                }
            }

            #Individual user cleanup
            ForEach ($Record in $SeniorFilter) {
                Write-Host "`nTaking action for following super record:"
                Write-Host $Record
                If ($Record.MatchType -eq 'Google') {
                    $SeniorLog = Step-Five9CleanupGoogle $SeniorUserReport $Record.Username $Record.Record $Record.DNIS 'Senior'
                    $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Log' -Value $SeniorLog

                    #Store data in GSheets
                    $SeniorDisabledLog = (Add-GoogleOffboardLog $Record $GoogleAPICreds $Offboarding_SheetID $Disabled_SheetTab)
                    $SeniorOffboardLog = (Add-GoogleOffboardRecord $GoogleAPICreds $Record $Senior_SheetID 'Senior')
                    $SeniorStationIDLog = (Remove-AllStationIDs $GoogleAPICreds $Senior_SheetID $Record.Username 'Senior')

                    #Report if data was stored successfully in Gsheets
                    if ($SeniorDisabledLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'FAILURE'
                    }
                    if ($SeniorOffboardLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google Offboarded Record' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google Offboarded Record' -Value 'FAILURE'
                    }
                    if ($SeniorStationIDLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google StationID Removed' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google StationID Removed' -Value 'FAILURE'
                    }
                    
                    [void]$SeniorRecordIndex.add($Record.Index) #Store the index number to delete in bulk at the end of Senior steps
                }
                ElseIf ($Record.MatchType -eq 'Five9') {
                    $SeniorLog = Step-Five9CleanupFive9 $Record
                    $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Log' -Value $SeniorLog

                    #Store data in GSheets
                    $SeniorDisabledLog = Add-GoogleOffboardLog $Record $GoogleAPICreds $Offboarding_SheetID $Disabled_SheetTab
                    #Report if data was stored successfully in Gsheets
                    if ($SeniorDisabledLog) {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'FAILURE'
                    }
                }
            }
            if ($SeniorRecordIndex) {
                $SeniorRemoveRecord = (Remove-GoogleContactRecord $SeniorRecordIndex $GoogleAPICreds $Senior_SheetID 'Senior')
                if ($SeniorRemoveRecord -eq 'TRUE') {
                    ForEach ($Index in $SeniorRecordIndex) {
                        $SuperRecord | Where-Object {($_.Index -eq $Index) -and ($_.Domain -eq 'Senior')} |
                            Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'SUCCESS'
                    }
                    $SuperRecord | Where-Object {(!$_.'Google Record Removed') -and ($_.Domain -eq 'Senior')} |
                        Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'FAILURE'
                }
                else {
                    $SuperRecord | Where-Object {$_.Domain -eq 'Senior'} |
                        Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'FAILURE'
                }
            }
        }

        #UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC-UHC#
        #####################################################################################################
        if ($UHCFilter) {
            Connect-Five9AdminWebService -Credential $UHCCreds #Connection to Five9 UHC (required before next actions)

            $UHCDNISList = Step-DNISProcessing $UHCFilter
            $UHCRecordIndex = New-Object System.Collections.ArrayList($null) #This is used to track indexes for each record

            #Grab Five9 UHC data
            if ($UHCDNISList) {
                Write-Host "Fetching UHC DNIS report..." -ForegroundColor Yellow
                $UHCDNISReport = Get-Five9DNISReport
            }
            if (!$UHCUserReport) {
                Write-Host "Fetching UHC User report..." -ForegroundColor Yellow
                $UHCUserReport = Get-Five9UserReport
            }

            #Move DNIS to deactivated campaign
            if ($UHCDNISList) {
                $UHCDNISActionList = Get-Five9Campaigns $UHCDNISList $UHCDNISReport
                if ($UHCDNISActionList -ne 'FALSE') {
                    try {
                        Remove-Five9DNIS $UHCDNISActionList
                        $SuperRecord | Where-Object {$_.Domain -eq 'UHC'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'SUCCESS'
                    }
                    catch {
                        $_
                        $SuperRecord | Where-Object {$_.Domain -eq 'UHC'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'FAILURE'
                    }
                }
                else {
                    $SuperRecord | Where-Object {$_.Domain -eq 'UHC'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'ALREADY IN DESTINATION CAMPAIGN'
                }
            }

            #Individual user cleanup
            ForEach ($Record in $UHCFilter) {
                Write-Host "`nTaking action for following super record:"
                Write-Host $Record
                If ($Record.MatchType -eq 'Google') {
                    $UHCLog = Step-Five9CleanupGoogle $UHCUserReport $Record.Username $Record.Record $Record.DNIS 'UHC'
                    $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Log' -Value $UHCLog

                    #Store data in GSheets
                    $UHCDisabledLog = (Add-GoogleOffboardLog $Record $GoogleAPICreds $Offboarding_SheetID $Disabled_SheetTab)
                    $UHCOffboardLog = (Add-GoogleOffboardRecord $GoogleAPICreds $Record $UHC_SheetID 'UHC')
                    $UHCStationIDLog = (Remove-AllStationIDs $GoogleAPICreds $UHC_SheetID $Record.Username 'UHC')

                    #Report if data was stored successfully in Gsheets
                    if ($UHCDisabledLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'FAILURE'
                    }
                    if ($UHCOffboardLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google Offboarded Record' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google Offboarded Record' -Value 'FAILURE'
                    }
                    if ($UHCStationIDLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google StationID Removed' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google StationID Removed' -Value 'FAILURE'
                    }
                    
                    [void]$UHCRecordIndex.add($Record.Index) #Store the index number to delete in bulk at the end of UHC steps
                }
                ElseIf ($Record.MatchType -eq 'Five9') {
                    $UHCLog = Step-Five9CleanupFive9 $Record
                    $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Log' -Value $UHCLog

                    #Store data in GSheets
                    $UHCDisabledLog = Add-GoogleOffboardLog $Record $GoogleAPICreds $Offboarding_SheetID $Disabled_SheetTab
                    #Report if data was stored successfully in Gsheets
                    if ($UHCDisabledLog) {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'FAILURE'
                    }
                }
            }
            if ($UHCRecordIndex) {
                $UHCRemoveRecord = (Remove-GoogleContactRecord $UHCRecordIndex $GoogleAPICreds $UHC_SheetID 'UHC')
                if ($UHCRemoveRecord -eq 'TRUE') {
                    ForEach ($Index in $UHCRecordIndex) {
                        $SuperRecord | Where-Object {($_.Index -eq $Index) -and ($_.Domain -eq 'UHC')} |
                            Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'SUCCESS'
                    }
                    $SuperRecord | Where-Object {(!$_.'Google Record Removed') -and ($_.Domain -eq 'UHC')} |
                        Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'FAILURE'
                }
                else {
                    $SuperRecord | Where-Object {$_.Domain -eq 'UHC'} |
                        Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'FAILURE'
                }
            }
        }

        #CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA-CCA#
        #####################################################################################################
        if ($CCAFilter) {
            Connect-Five9AdminWebService -Credential $CCACreds #Connection to Five9 CCA (required before next actions)

            $CCADNISList = Step-DNISProcessing $CCAFilter
            $CCARecordIndex = New-Object System.Collections.ArrayList($null) #This is used to track indexes for each record

            #Grab Five9 CCA data
            if ($CCADNISList) {
                Write-Host "Fetching CCA DNIS report..." -ForegroundColor Yellow
                $CCADNISReport = Get-Five9DNISReport
            }
            if (!$CCAUserReport) {
                Write-Host "Fetching CCA User report..." -ForegroundColor Yellow
                $CCAUserReport = Get-Five9UserReport
            }

            #Move DNIS to deactivated campaign
            if ($CCADNISList) {
                $CCADNISActionList = Get-Five9Campaigns $CCADNISList $CCADNISReport
                if ($CCADNISActionList -ne 'FALSE') {
                    try {
                        Remove-Five9DNIS $CCADNISActionList
                        $SuperRecord | Where-Object {$_.Domain -eq 'CCA'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'SUCCESS'
                    }
                    catch {
                        $_
                        $SuperRecord | Where-Object {$_.Domain -eq 'CCA'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'FAILURE'
                    }
                }
                else {
                    $SuperRecord | Where-Object {$_.Domain -eq 'CCA'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'ALREADY IN DESTINATION CAMPAIGN'
                }
            }

            #Individual user cleanup
            ForEach ($Record in $CCAFilter) {
                Write-Host "`nTaking action for following super record:"
                Write-Host $Record
                If ($Record.MatchType -eq 'Google') {
                    $CCALog = Step-Five9CleanupGoogle $CCAUserReport $Record.Username $Record.Record $Record.DNIS 'CCA'
                    $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Log' -Value $CCALog

                    #Store data in GSheets
                    $CCADisabledLog = (Add-GoogleOffboardLog $Record $GoogleAPICreds $Offboarding_SheetID $Disabled_SheetTab)
                    $CCAOffboardLog = (Add-GoogleOffboardRecord $GoogleAPICreds $Record $CCA_SheetID 'CCA')
                    $CCAStationIDLog = (Remove-AllStationIDs $GoogleAPICreds $CCA_SheetID $Record.Username 'CCA')

                    #Report if data was stored successfully in Gsheets
                    if ($CCADisabledLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'FAILURE'
                    }
                    if ($CCAOffboardLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google Offboarded Record' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google Offboarded Record' -Value 'FAILURE'
                    }
                    if ($CCAStationIDLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google StationID Removed' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google StationID Removed' -Value 'FAILURE'
                    }
                    
                    [void]$CCARecordIndex.add($Record.Index) #Store the index number to delete in bulk at the end of CCA steps
                }
                ElseIf ($Record.MatchType -eq 'Five9') {
                    $CCALog = Step-Five9CleanupFive9 $Record
                    $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Log' -Value $CCALog

                    #Store data in GSheets
                    $CCADisabledLog = Add-GoogleOffboardLog $Record $GoogleAPICreds $Offboarding_SheetID $Disabled_SheetTab
                    #Report if data was stored successfully in Gsheets
                    if ($CCADisabledLog) {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'FAILURE'
                    }
                }
            }
            if ($CCARecordIndex) {
                $CCARemoveRecord = (Remove-GoogleContactRecord $CCARecordIndex $GoogleAPICreds $CCA_SheetID 'CCA')
                if ($CCARemoveRecord -eq 'TRUE') {
                    ForEach ($Index in $CCARecordIndex) {
                        $SuperRecord | Where-Object {($_.Index -eq $Index) -and ($_.Domain -eq 'CCA')} |
                            Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'SUCCESS'
                    }
                    $SuperRecord | Where-Object {(!$_.'Google Record Removed') -and ($_.Domain -eq 'CCA')} |
                        Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'FAILURE'
                }
                else {
                    $SuperRecord | Where-Object {$_.Domain -eq 'CCA'} |
                        Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'FAILURE'
                }
            }
        }

        #SQAH-SQAH-SQAH-SQAH-SQAH-SQAH-SQAH-SQAH-SQAH-SQAH-SQAH-SQAH-SQAH-SQAH-SQAH-SQAH-SQAH-SQAH-SQAH-SQAH#
        #####################################################################################################
        if ($SQAHFilter) {
            Connect-Five9AdminWebService -Credential $SQAHCreds #Connection to Five9 SQAH (required before next actions)

            $SQAHDNISList = Step-DNISProcessing $SQAHFilter
            $SQAHRecordIndex = New-Object System.Collections.ArrayList($null) #This is used to track indexes for each record

            #Grab Five9 SQAH data
            if ($SQAHDNISList) {
                Write-Host "Fetching SQAH DNIS report..." -ForegroundColor Yellow
                $SQAHDNISReport = Get-Five9DNISReport
            }
            if (!$SQAHUserReport) {
                Write-Host "Fetching SQAH User report..." -ForegroundColor Yellow
                $SQAHUserReport = Get-Five9UserReport
            }

            #Move DNIS to deactivated campaign
            if ($SQAHDNISList) {
                $SQAHDNISActionList = Get-Five9Campaigns $SQAHDNISList $SQAHDNISReport
                if ($SQAHDNISActionList -ne 'FALSE') {
                    try {
                        Remove-Five9DNIS $SQAHDNISActionList
                        $SuperRecord | Where-Object {$_.Domain -eq 'SQAH'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'SUCCESS'
                    }
                    catch {
                        $_
                        $SuperRecord | Where-Object {$_.Domain -eq 'SQAH'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'FAILURE'
                    }
                }
                else {
                    $SuperRecord | Where-Object {$_.Domain -eq 'SQAH'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'ALREADY IN DESTINATION CAMPAIGN'
                }
            }

            #Individual user cleanup
            ForEach ($Record in $SQAHFilter) {
                Write-Host "`nTaking action for following super record:"
                Write-Host $Record
                If ($Record.MatchType -eq 'Google') {
                    $SQAHLog = Step-Five9CleanupGoogle $SQAHUserReport $Record.Username $Record.Record $Record.DNIS 'SQAH'
                    $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Log' -Value $SQAHLog

                    #Store data in GSheets
                    $SQAHDisabledLog = (Add-GoogleOffboardLog $Record $GoogleAPICreds $Offboarding_SheetID $Disabled_SheetTab)
                    $SQAHOffboardLog = (Add-GoogleOffboardRecord $GoogleAPICreds $Record $SQAH_SheetID 'SQAH')
                    $SQAHStationIDLog = (Remove-AllStationIDs $GoogleAPICreds $SQAH_SheetID $Record.Username 'SQAH')

                    #Report if data was stored successfully in Gsheets
                    if ($SQAHDisabledLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'FAILURE'
                    }
                    if ($SQAHOffboardLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google Offboarded Record' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google Offboarded Record' -Value 'FAILURE'
                    }
                    if ($SQAHStationIDLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google StationID Removed' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google StationID Removed' -Value 'FAILURE'
                    }
                    
                    [void]$SQAHRecordIndex.add($Record.Index) #Store the index number to delete in bulk at the end of SQAH steps
                }
                ElseIf ($Record.MatchType -eq 'Five9') {
                    $SQAHLog = Step-Five9CleanupFive9 $Record
                    $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Log' -Value $SQAHLog

                    #Store data in GSheets
                    $SQAHDisabledLog = Add-GoogleOffboardLog $Record $GoogleAPICreds $Offboarding_SheetID $Disabled_SheetTab
                    #Report if data was stored successfully in Gsheets
                    if ($SQAHDisabledLog) {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'FAILURE'
                    }
                }
            }
            if ($SQAHRecordIndex) {
                $SQAHRemoveRecord = (Remove-GoogleContactRecord $SQAHRecordIndex $GoogleAPICreds $SQAH_SheetID 'SQAH')
                if ($SQAHRemoveRecord -eq 'TRUE') {
                    ForEach ($Index in $SQAHRecordIndex) {
                        $SuperRecord | Where-Object {($_.Index -eq $Index) -and ($_.Domain -eq 'SQAH')} |
                            Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'SUCCESS'
                    }
                    $SuperRecord | Where-Object {(!$_.'Google Record Removed') -and ($_.Domain -eq 'SQAH')} |
                        Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'FAILURE'
                }
                else {
                    $SuperRecord | Where-Object {$_.Domain -eq 'SQAH'} |
                        Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'FAILURE'
                }
            }
        }

        #HEALTH-HEALTH-HEALTH-HEALTH-HEALTH-HEALTH-HEALTH-HEALTH-HEALTH-HEALTH-HEALTH-HEALTH-HEALTH-HEALTH-#
        ####################################################################################################
        if ($HealthFilter) {
            Connect-Five9AdminWebService -Credential $HealthCreds #Connection to Five9 Health (required before next actions)

            $HealthDNISList = Step-DNISProcessing $HealthFilter
            $HealthRecordIndex = New-Object System.Collections.ArrayList($null) #This is used to track indexes for each record

            #Grab Five9 Health data
            if ($HealthDNISList) {
                Write-Host "Fetching Health DNIS report..." -ForegroundColor Yellow
                $HealthDNISReport = Get-Five9DNISReport
            }
            if (!$HealthUserReport) {
                Write-Host "Fetching Health User report..." -ForegroundColor Yellow
                $HealthUserReport = Get-Five9UserReport
            }

            #Move DNIS to deactivated campaign
            if ($HealthDNISList) {
                $HealthDNISActionList = Get-Five9Campaigns $HealthDNISList $HealthDNISReport
                if ($HealthDNISActionList -ne 'FALSE') {
                    try {
                        Remove-Five9DNIS $HealthDNISActionList
                        $SuperRecord | Where-Object {$_.Domain -eq 'Health'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'SUCCESS'
                    }
                    catch {
                        $_
                        $SuperRecord | Where-Object {$_.Domain -eq 'Health'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'FAILURE'
                    }
                }
                else {
                    $SuperRecord | Where-Object {$_.Domain -eq 'Health'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'ALREADY IN DESTINATION CAMPAIGN'
                }
            }

            #Individual user cleanup
            ForEach ($Record in $HealthFilter) {
                Write-Host "`nTaking action for following super record:"
                Write-Host $Record
                If ($Record.MatchType -eq 'Google') {
                    $HealthLog = Step-Five9CleanupGoogle $HealthUserReport $Record.Username $Record.Record $Record.DNIS 'Health'
                    $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Log' -Value $HealthLog

                    #Store data in GSheets
                    $HealthDisabledLog = (Add-GoogleOffboardLog $Record $GoogleAPICreds $Offboarding_SheetID $Disabled_SheetTab)
                    $HealthOffboardLog = (Add-GoogleOffboardRecord $GoogleAPICreds $Record $Health_SheetID 'Health')
                    $HealthStationIDLog = (Remove-AllStationIDs $GoogleAPICreds $Health_SheetID $Record.Username 'Health')

                    #Report if data was stored successfully in Gsheets
                    if ($HealthDisabledLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'FAILURE'
                    }
                    if ($HealthOffboardLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google Offboarded Record' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google Offboarded Record' -Value 'FAILURE'
                    }
                    if ($HealthStationIDLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google StationID Removed' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google StationID Removed' -Value 'FAILURE'
                    }
                    
                    [void]$HealthRecordIndex.add($Record.Index) #Store the index number to delete in bulk at the end of Health steps
                }
                ElseIf ($Record.MatchType -eq 'Five9') {
                    $HealthLog = Step-Five9CleanupFive9 $Record
                    $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Log' -Value $HealthLog

                    #Store data in GSheets
                    $HealthDisabledLog = Add-GoogleOffboardLog $Record $GoogleAPICreds $Offboarding_SheetID $Disabled_SheetTab
                    #Report if data was stored successfully in Gsheets
                    if ($HealthDisabledLog) {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'FAILURE'
                    }
                }
            }
            if ($HealthRecordIndex) {
                $HealthRemoveRecord = (Remove-GoogleContactRecord $HealthRecordIndex $GoogleAPICreds $Health_SheetID 'Health')
                if ($HealthRemoveRecord -eq 'TRUE') {
                    ForEach ($Index in $HealthRecordIndex) {
                        $SuperRecord | Where-Object {($_.Index -eq $Index) -and ($_.Domain -eq 'Health')} |
                            Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'SUCCESS'
                    }
                    $SuperRecord | Where-Object {(!$_.'Google Record Removed') -and ($_.Domain -eq 'Health')} |
                        Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'FAILURE'
                }
                else {
                    $SuperRecord | Where-Object {$_.Domain -eq 'Health'} |
                        Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'FAILURE'
                }
            }
        }

        #LIFE-LIFE-LIFE-LIFE-LIFE-LIFE-LIFE-LIFE-LIFE-LIFE-LIFE-LIFE-LIFE-LIFE-LIFE-LIFE-LIFE-LIFE-LIFE-LIFE#
        #####################################################################################################
        if ($LifeFilter) {
            Connect-Five9AdminWebService -Credential $LifeCreds #Connection to Five9 Life (required before next actions)

            $LifeDNISList = Step-DNISProcessing $LifeFilter
            $LifeRecordIndex = New-Object System.Collections.ArrayList($null) #This is used to track indexes for each record

            #Grab Five9 Life data
            if ($LifeDNISList) {
                Write-Host "Fetching Life DNIS report..." -ForegroundColor Yellow
                $LifeDNISReport = Get-Five9DNISReport
            }
            if (!$LifeUserReport) {
                Write-Host "Fetching Life User report..." -ForegroundColor Yellow
                $LifeUserReport = Get-Five9UserReport
            }

            #Move DNIS to deactivated campaign
            if ($LifeDNISList) {
                $LifeDNISActionList = Get-Five9Campaigns $LifeDNISList $LifeDNISReport
                if ($LifeDNISActionList -ne 'FALSE') {
                    try {
                        Remove-Five9DNIS $LifeDNISActionList
                        $SuperRecord | Where-Object {$_.Domain -eq 'Life'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'SUCCESS'
                    }
                    catch {
                        $_
                        $SuperRecord | Where-Object {$_.Domain -eq 'Life'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'FAILURE'
                    }
                }
                else {
                    $SuperRecord | Where-Object {$_.Domain -eq 'Life'} |
                            Add-Member -MemberType NoteProperty -Name 'Numbers Moved' -Value 'ALREADY IN DESTINATION CAMPAIGN'
                }
            }

            #Individual user cleanup
            ForEach ($Record in $LifeFilter) {
                Write-Host "`nTaking action for following super record:"
                Write-Host $Record
                If ($Record.MatchType -eq 'Google') {
                    $LifeLog = Step-Five9CleanupGoogle $LifeUserReport $Record.Username $Record.Record $Record.DNIS 'Life'
                    $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Log' -Value $LifeLog

                    #Store data in GSheets
                    $LifeDisabledLog = (Add-GoogleOffboardLog $Record $GoogleAPICreds $Offboarding_SheetID $Disabled_SheetTab)
                    $LifeOffboardLog = (Add-GoogleOffboardRecord $GoogleAPICreds $Record $Life_SheetID 'Life')
                    $LifeStationIDLog = (Remove-AllStationIDs $GoogleAPICreds $Life_SheetID $Record.Username 'Life')

                    #Report if data was stored successfully in Gsheets
                    if ($LifeDisabledLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'FAILURE'
                    }
                    if ($LifeOffboardLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google Offboarded Record' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google Offboarded Record' -Value 'FAILURE'
                    }
                    if ($LifeStationIDLog -eq 'TRUE') {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google StationID Removed' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Google StationID Removed' -Value 'FAILURE'
                    }
                    
                    [void]$LifeRecordIndex.add($Record.Index) #Store the index number to delete in bulk at the end of Life steps
                }
                ElseIf ($Record.MatchType -eq 'Five9') {
                    $LifeLog = Step-Five9CleanupFive9 $Record
                    $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Log' -Value $LifeLog

                    #Store data in GSheets
                    $LifeDisabledLog = Add-GoogleOffboardLog $Record $GoogleAPICreds $Offboarding_SheetID $Disabled_SheetTab
                    #Report if data was stored successfully in Gsheets
                    if ($LifeDisabledLog) {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'SUCCESS'
                    }
                    else {
                        $SuperRecord | Where-Object {$_.Username -eq $Record.Username} |
                            Add-Member -MemberType NoteProperty -Name 'Deletion Timer' -Value 'FAILURE'
                    }
                }
            }
            if ($LifeRecordIndex) {
                $LifeRemoveRecord = (Remove-GoogleContactRecord $LifeRecordIndex $GoogleAPICreds $Life_SheetID 'Life')
                if ($LifeRemoveRecord -eq 'TRUE') {
                    ForEach ($Index in $LifeRecordIndex) {
                        $SuperRecord | Where-Object {($_.Index -eq $Index) -and ($_.Domain -eq 'Life')} |
                            Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'SUCCESS'
                    }
                    $SuperRecord | Where-Object {(!$_.'Google Record Removed') -and ($_.Domain -eq 'Life')} |
                        Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'FAILURE'
                }
                else {
                    $SuperRecord | Where-Object {$_.Domain -eq 'Life'} |
                        Add-Member -MemberType NoteProperty -Name 'Google Record Removed' -Value 'FAILURE'
                }
            }
        }
        ######################################################################################################################
        ######################################################################################################################



        #################################################
        #Email reporting section
        #################################################

        $FinalReport = New-Object System.Collections.ArrayList($null) #This is used to track all of the data for the matched user
        $DNISErrorReport = New-Object System.Collections.ArrayList($null) #This stores all numbers that reported an error in matching
        $SeniorErrors = ($SeniorDNISActionList | ForEach-Object {If ($_.Action -eq 'Error') {$_}})
        if ($SeniorErrors) {
            [void]$DNISErrorReport.add($SeniorErrors)
        }
        $UHCErrors = ($UHCDNISActionList | ForEach-Object {If ($_.Action -eq 'Error') {$_}})
        if ($UHCErrors) {
            [void]$DNISErrorReport.add($UHCErrors)
        }
        $CCAErrors = ($CCADNISActionList | ForEach-Object {If ($_.Action -eq 'Error') {$_}})
        if ($CCAErrors) {
            [void]$DNISErrorReport.add($CCAErrors)
        }
        $SQAHErrors = ($SQAHDNISActionList | ForEach-Object {If ($_.Action -eq 'Error') {$_}})
        if ($SQAHErrors) {
            [void]$DNISErrorReport.add($SQAHErrors)
        }
        $HealthErrors = ($HealthDNISActionList | ForEach-Object {If ($_.Action -eq 'Error') {$_}})
        if ($HealthErrors) {
            [void]$DNISErrorReport.add($HealthErrors)
        }
        $LifeErrors = ($LifeDNISActionList | ForEach-Object {If ($_.Action -eq 'Error') {$_}})
        if ($LifeErrors) {
            [void]$DNISErrorReport.add($LifeErrors)
        }
        ForEach ($Record in $SuperRecord) {
            #Modify the final report based on match type
            if ($Record.MatchType -eq 'Google') {
                #This prevents blank spaces for final formatting
                if (($Record.DNIS -ne '') -and ($DNISErrorReport.DNIS -notcontains $Record.DNIS)) {
                    $DNISSuccess = $Record.DNIS
                    $DNISFail = $null
                }
                elseif (($Record.DNIS -ne '') -and ($DNISErrorReport.DNIS -contains $Record.DNIS)) {
                    $DNISSuccess = $null
                    $DNISFail = $Record.DNIS
                }
                else {
                    $DNISSuccess = $null
                    $DNISFail = $null
                }
                if (($Record.TempDNIS -ne '') -and ($DNISErrorReport.DNIS -notcontains $Record.TempDNIS)) {
                    $TempDNISSuccess = $Record.TempDNIS
                    $TempDNISFail = $null
                }
                elseif (($Record.TempDNIS -ne '') -and ($DNISErrorReport.DNIS -contains $Record.TempDNIS)) {
                    $TempDNISSuccess = $null
                    $TempDNISFail = $Record.TempDNIS
                }
                else {
                    $TempDNISSuccess = $null
                    $TempDNISFail = $null
                }
                if ($Record.Loopback -ne '') {
                    $LoopbackSuccess = $Record.Loopback
                    $LoopbackFail = $null
                }
                elseif (($Record.Loopback -ne '') -and ($DNISErrorReport.DNIS -contains $Record.Loopback)) {
                    $LoopbackSuccess = $null
                    $LoopbackFail = $Record.Loopback
                }
                else {
                    $LoopbackSuccess = $null
                }
                #This will report what numbers were moved
                if ($Record.'Numbers Moved' -eq 'SUCCESS') {
                    $NumbersSuccess = @($DNISSuccess, $TempDNISSuccess, $LoopbackSuccess, 'SUCCESS')
                    $NumbersFail = @($DNISFail, $TempDNISFail, $LoopbackFail)
                }
                elseif ($Record.'Numbers Moved' -eq 'ALREADY IN DESTINATION CAMPAIGN') {
                    $NumbersSuccess = @($DNISSuccess, $TempDNISSuccess, $LoopbackSuccess, 'ALREADY IN DESTINATION CAMPAIGN')
                    $NumbersFail = @($DNISFail, $TempDNISFail, $LoopbackFail)
                }
                else {
                    $NumbersSuccess = @('FAILURE')
                    $NumbersFail = @($DNISSuccess, $TempDNISSuccess, $LoopbackSuccess)
                }
                #This will report what numbers failed

                $FinalReport.add([PSCustomObject]@{
                    'Offboard Requester' = $Record.'Offboard Requester'
                    'Match Type' = $Record.MatchType
                    'Record Number' = $Record.Record
                    'Employee ID' = $Record.EID
                    'Username' = $Record.Username
                    'Numbers Moved' = $NumbersSuccess
                    'Numbers Failed' = $NumbersFail
                    'Agent Groups Removed' = $Record.Log.'Agent Group Removed'
                    'Domain' = $Record.Domain
                    'Five9 Account Disabled' = $Record.Log.Disabled
                    'Five9 Speed Dial' = $Record.Log.'Speed Dial Removed'
                    'Five9 Contact Record' = $Record.Log.'Contact Record'
                    'Deletion Timer' = $Record.'Deletion Timer'
                    'Google Offboarded Record' = $Record.'Google Offboarded Record'
                    'Google StationID Removed' = $Record.'Google StationID Removed'
                    'Google Record Removed' = $Record.'Google Record Removed'
                }) | Out-Null
            }
            if ($Record.MatchType -eq 'Five9') {
                $FinalReport.add([PSCustomObject]@{
                    'Offboard Requester' = $Record.'Offboard Requester'
                    'Match Type' = $Record.MatchType
                    'Username' = $Record.Username
                    'Domain' = $Record.Domain
                    'Five9 Account Disabled' = $Record.Log.Disabled
                    'Agent Groups Removed' = $Record.Log.'Agent Group Removed'
                    'Deletion Timer' = $Record.'Deletion Timer'
                }) | Out-Null
            }
        }
        $Requesters = $FinalReport.'Offboard Requester'
        $UniqueEmails = ($Requesters | Get-Unique)
        ForEach ($Email in $UniqueEmails) {
            $EmailList = ($FinalReport | ForEach-Object {If ($_.'Offboard Requester' -eq $Email) {$_}})
            $EmailData = New-Object System.Collections.ArrayList($null) #This is used to combine all reports for one requester into one email
            ###This part is really ugly but it was unavoidable to format the report properly
            ForEach ($Report in $EmailList) {
                if ($Report.'Match Type' -eq 'Google') {
                    $EmailBody = @'
<b>Match Type: </b>
'@ + $Report.'Match Type' + '<br>' +
@'
<b>Domain: </b>
'@ + $Report.Domain + '<br>' +
@'
<b>Record Number: </b>
'@ + $Report.'Record Number' + '<br>' +
@'
<b>Employee ID: </b>
'@ + $Report.'Employee ID' + '<br>' +
@'
<b>Username: </b>
'@ + $Report.Username + '<br>' +
@'
<b>Numbers Moved: </b>
'@ + $Report.'Numbers Moved' + '<br>' +
@'
<b>Numbers Failed: </b>
'@ + $Report.'Numbers Failed' + '<br>' +
@'
<b>Five9 Account Disabled: </b>
'@ + $Report.'Five9 Account Disabled' + '<br>' +
@'
<b>Five9 Speed Dial: </b>
'@ + $Report.'Five9 Speed Dial' + '<br>' +
@'
<b>Five9 Contact Record: </b>
'@ + $Report.'Five9 Contact Record' + '<br>' +
@'
<b>Five9 Agent Groups Removed: </b>
'@ + $Report.'Agent Groups Removed' + '<br>' +
@'
<b>Deletion Timer: </b>
'@ + $Report.'Deletion Timer' + '<br>' +
@'
<b>Google Offboarded Record: </b>
'@ + $Report.'Google Offboarded Record' + '<br>' +
@'
<b>Google StationID Removed: </b>
'@ + $Report.'Google StationID Removed' + '<br>' +
@'
<b>Google Record Removed: </b>
'@ + $Report.'Google Record Removed' + "<br><br>"
                }
                if ($Report.'Match Type' -eq 'Five9') {
                    $EmailBody = @'
<b>Match Type: </b>
'@ + $Report.'Match Type' + '<br>' +
@'
<b>Domain: </b>
'@ + $Report.Domain + '<br>' +
@'
<b>Username: </b>
'@ + $Report.Username + '<br>' +
@'
<b>Five9 Account Disabled: </b>
'@ + $Report.'Five9 Account Disabled' + '<br>' +
@'
<b>Agent Groups Removed: </b>
'@ + $Report.'Agent Groups Removed' + '<br>' +
@'
<b>Deletion Timer: </b>
'@ + $Report.'Deletion Timer' + "<br><br>"
                }
                [void]$EmailData.add($EmailBody)
            }
            $EmailReport = ($EmailData | Out-String)
            Send-EmailReport $GoogleAPICreds $Email $EmailReport
        }

        #Delete completed lines
        if ($emptyLines) {
            Write-Host "`nDeleting empty lines..." -ForegroundColor Yellow
            #Delete empty lines in offboarding sheet
            Set-DeleteLine `
                -token $GoogleAPICreds `
                -indexes $emptyLines `
                -sheetName $Offboarding_SheetTab `
                -sheetID $Offboarding_SheetID | Out-Null
        }
    }
}
catch {
    $_
    #WIP -- Add error emailer here later
}
Write-Host ($FinalReport | Out-String)
#Stop logging
Stop-Transcript