#This function will return users with the same name in each domain
function Get-Five9Username {
    param (
        [Parameter(Mandatory=$true)][string]$name,
        [Parameter(Mandatory=$false)]$SeniorReport,
        [Parameter(Mandatory=$false)]$UHCReport,
        [Parameter(Mandatory=$false)]$CCAReport,
        [Parameter(Mandatory=$false)]$SQAHReport,
        [Parameter(Mandatory=$false)]$HealthReport,
        [Parameter(Mandatory=$false)]$LifeReport
    )

    $MatchedUsers = New-Object System.Collections.ArrayList($null)
    $SeniorFilter = ($SeniorReport | ForEach-Object {If ($_.'AGENT NAME' -eq $name) {$_}})
    ForEach ($Match in $SeniorFilter) {
        $Name = $Match.'AGENT FIRST NAME' + ' ' + $Match.'AGENT LAST NAME'
        [void]$MatchedUsers.add([PSCustomObject]@{
            'Username' = $Match.AGENT
            'Name' = $Name
            'Domain' = 'Senior'
        })
    }
    $UHCFilter = ($UHCReport | ForEach-Object {If ($_.'AGENT NAME' -eq $name) {$_}})
    ForEach ($Match in $UHCFilter) {
        $Name = $Match.'AGENT FIRST NAME' + ' ' + $Match.'AGENT LAST NAME'
        [void]$MatchedUsers.add([PSCustomObject]@{
            'Username' = $Match.AGENT
            'Name' = $Name
            'Domain' = 'UHC'
        })
    }
    $CCAFilter = ($CCAReport | ForEach-Object {If ($_.'AGENT NAME' -eq $name) {$_}})
    ForEach ($Match in $CCAFilter) {
        $Name = $Match.'AGENT FIRST NAME' + ' ' + $Match.'AGENT LAST NAME'
        [void]$MatchedUsers.add([PSCustomObject]@{
            'Username' = $Match.AGENT
            'Name' = $Name
            'Domain' = 'CCA'
        })
    }
    $SQAHFilter = ($SQAHReport | ForEach-Object {If ($_.'AGENT NAME' -eq $name) {$_}})
    ForEach ($Match in $SQAHFilter) {
        $Name = $Match.'AGENT FIRST NAME' + ' ' + $Match.'AGENT LAST NAME'
        [void]$MatchedUsers.add([PSCustomObject]@{
            'Username' = $Match.AGENT
            'Name' = $Name
            'Domain' = 'SQAH'
        })
    }
    $HealthFilter = ($HealthReport | ForEach-Object {If ($_.'AGENT NAME' -eq $name) {$_}})
    ForEach ($Match in $HealthFilter) {
        $Name = $Match.'AGENT FIRST NAME' + ' ' + $Match.'AGENT LAST NAME'
        [void]$MatchedUsers.add([PSCustomObject]@{
            'Username' = $Match.AGENT
            'Name' = $Name
            'Domain' = 'Health'
        })
    }
    $LifeFilter = ($LifeReport | ForEach-Object {If ($_.'AGENT NAME' -eq $name) {$_}})
    ForEach ($Match in $LifeFilter) {
        $Name = $Match.'AGENT FIRST NAME' + ' ' + $Match.'AGENT LAST NAME'
        [void]$MatchedUsers.add([PSCustomObject]@{
            'Username' = $Match.AGENT
            'Name' = $Name
            'Domain' = 'Life'
        })
    }
    return $MatchedUsers
}

function Get-Five9UserDomain {
    param (
        [Parameter(Mandatory=$true)][string]$username,
        [Parameter(Mandatory=$true)]$SeniorReport,
        [Parameter(Mandatory=$true)]$UHCReport,
        [Parameter(Mandatory=$true)]$CCAReport,
        [Parameter(Mandatory=$true)]$SQAHReport,
        [Parameter(Mandatory=$true)]$HealthReport,
        [Parameter(Mandatory=$true)]$LifeReport
    )
    
    $MatchedUsers = New-Object System.Collections.ArrayList($null)
    $SeniorFilter = ($SeniorReport | ForEach-Object {If ($_.AGENT -eq $username) {$_}})
    ForEach ($Match in $SeniorFilter) {
        $Name = $Match.'AGENT FIRST NAME' + ' ' + $Match.'AGENT LAST NAME'
        [void]$MatchedUsers.add([PSCustomObject]@{
            'Username' = $Match.AGENT
            'Name' = $Name
            'Domain' = 'Senior'
        })
    }
    $UHCFilter = ($UHCReport | ForEach-Object {If ($_.AGENT -eq $username) {$_}})
    ForEach ($Match in $UHCFilter) {
        $Name = $Match.'AGENT FIRST NAME' + ' ' + $Match.'AGENT LAST NAME'
        [void]$MatchedUsers.add([PSCustomObject]@{
            'Username' = $Match.AGENT
            'Name' = $Name
            'Domain' = 'UHC'
        })
    }
    $CCAFilter = ($CCAReport | ForEach-Object {If ($_.AGENT -eq $username) {$_}})
    ForEach ($Match in $CCAFilter) {
        $Name = $Match.'AGENT FIRST NAME' + ' ' + $Match.'AGENT LAST NAME'
        [void]$MatchedUsers.add([PSCustomObject]@{
            'Username' = $Match.AGENT
            'Name' = $Name
            'Domain' = 'CCA'
        })
    }
    $SQAHFilter = ($SQAHReport | ForEach-Object {If ($_.AGENT -eq $username) {$_}})
    ForEach ($Match in $SQAHFilter) {
        $Name = $Match.'AGENT FIRST NAME' + ' ' + $Match.'AGENT LAST NAME'
        [void]$MatchedUsers.add([PSCustomObject]@{
            'Username' = $Match.AGENT
            'Name' = $Name
            'Domain' = 'SQAH'
        })
    }
    $HealthFilter = ($HealthReport | ForEach-Object {If ($_.AGENT -eq $username) {$_}})
    ForEach ($Match in $HealthFilter) {
        $Name = $Match.'AGENT FIRST NAME' + ' ' + $Match.'AGENT LAST NAME'
        [void]$MatchedUsers.add([PSCustomObject]@{
            'Username' = $Match.AGENT
            'Name' = $Name
            'Domain' = 'Health'
        })
    }
    $LifeFilter = ($LifeReport | ForEach-Object {If ($_.AGENT -eq $username) {$_}})
    ForEach ($Match in $LifeFilter) {
        $Name = $Match.'AGENT FIRST NAME' + ' ' + $Match.'AGENT LAST NAME'
        [void]$MatchedUsers.add([PSCustomObject]@{
            'Username' = $Match.AGENT
            'Name' = $Name
            'Domain' = 'Life'
        })
    }
    return $MatchedUsers
}

function Get-Five9UserReport {
    $id = Start-Five9Report `
        -FolderName "API Reports" `
        -ReportName "Agents Information"
    $users = Get-Five9ReportResult `
        -Identifier $id `
        -WaitSeconds 5
    return $users
}

function Get-Five9DNISReport {
    $id = Start-Five9Report `
        -FolderName "API Reports" `
        -ReportName "DNIS by Campaign"
    $DNIS = Get-Five9ReportResult `
        -Identifier $id `
        -WaitSeconds 5
    return $DNIS
}

function Get-Five9Campaigns ($NumbersList, $DNISReport) {
    $DNISChange = New-Object System.Collections.ArrayList($null) #This stores a list of actions needed for each DNIS
    ForEach ($Item in $NumbersList) {
        $Numbers = @($Item.DNIS, $Item.TempDNIS, $Item.Loopback)
        $DestinationCampaign = $Item.'Destination Campaign'
        ForEach ($Number in $Numbers) {
            if (($Number -eq '') -or ($Null -eq $Number)) {
                Continue
            }
            $filterResults = ($DNISReport | ForEach-Object {If ($_.DNIS -eq $Number) {$_}})
            $CurrentCampaign = $filterResults.CAMPAIGN
            if (!$filterResults) {
                Write-Host "$Number wasn't found in target domain. That's weird..."
                $DNISChange.add([PSCustomObject]@{
                    'DNIS' = $Number
                    'Campaign' = $DestinationCampaign
                    'Action' = 'Error'
                }) | Out-Null
            }
            elseif ($CurrentCampaign -eq "[None]") {
                Write-Host "Adding $Number to Add List for $DestinationCampaign."
                $DNISChange.add([PSCustomObject]@{
                    'DNIS' = $Number
                    'Campaign' = $DestinationCampaign
                    'Action' = 'Add'
                }) | Out-Null
            }
            elseif (($CurrentCampaign) -eq ($DestinationCampaign)) {
                Write-Host "$Number is already in $DestinationCampaign."
            }
            else {
                Write-Host "Adding $Number to Remove List for $CurrentCampaign."
                $DNISChange.add([PSCustomObject]@{
                    'DNIS' = $Number
                    'Campaign' = $CurrentCampaign
                    'Action' = 'Remove'
                }) | Out-Null
                Write-Host "Adding $Number to Add List for $DestinationCampaign."
                $DNISChange.add([PSCustomObject]@{
                    'DNIS' = $Number
                    'Campaign' = $DestinationCampaign
                    'Action' = 'Add'
                }) | Out-Null
            }
        }
    }
    if ($DNISChange) {
        return $DNISChange
    }
    else {
        return 'FALSE'
    }
}

function Remove-Five9DNIS ($ChangeList) {
    #Move numbers based on actionlist created above
    $RemoveList = ($ChangeList | ForEach-Object {If ($_.Action -eq 'Remove') {$_}})
    $AddList = ($ChangeList | ForEach-Object {If ($_.Action -eq 'Add') {$_}})
    If ($RemoveList) {
        $CurrentCampaigns = $RemoveList.Campaign
        $UniqueCurrentCampaigns = ($CurrentCampaigns | Sort-Object | Get-Unique)
        ForEach ($Campaign in $UniqueCurrentCampaigns) {
            $RemovalListFilter = ($RemoveList | ForEach-Object {If ($_.Campaign -eq $Campaign) {$_}})
            $RemovalList = $RemovalListFilter.DNIS
            try {
                Remove-Five9CampaignDNIS -Name $Campaign -DNIS $RemovalList -Verbose
            }
            catch {
                $_
                throw {
                    "Could not complete the remove Campaign DNIS request."
                }
            }
        }
    }
    if ($AddList) {
        $DestinationCampaigns = $AddList.Campaign
        $UniqueDestinationCampaigns = ($DestinationCampaigns | Sort-Object | Get-Unique)
        ForEach($Campaign in $UniqueDestinationCampaigns) {
            $AddListFilter = ($AddList | ForEach-Object {If ($_.Campaign -eq $Campaign) {$_}})
            $AddList = $AddListFilter.DNIS
            try {
                Add-Five9CampaignDNIS -Name $Campaign -DNIS $AddList -Verbose
            }
            catch {
                $_
                throw {
                    "Could not complete the add Campaign DNIS request."
                }
            }
        }
    }
}

function Step-Five9CleanupGoogle ($Report, $Username, $ContactNumber, $DNIS, $Domain) {
    $SpeedDials = Get-Five9SpeedDial #Grab speed dials
    $ReportList = New-Object System.Collections.ArrayList($null) #Stores the status of each step for report summary

    #Disable user
    if ($Report.AGENT -contains $Username) {
        Write-Host "Disabling $Username in $Domain..."

        $userData = Get-Five9User $Username #Get agent user data
        $agentGroups = $userData.agentGroups #Create list of agent groups to be removed from

        #Add ZZ to the beginning of disabled users
        if ($userData.fullName -ne ('' -or $null)) {
            $firstCheck = $userData.firstName.substring(0, 2) #Checks the first two characters of a string to see if it's already ZZ
            $lastCheck = $userData.lastName.substring(0, 2) #Checks the first two characters of a string to see if it's already ZZ
            if ($firstCheck -ne 'ZZ') {
                $FirstName = 'ZZ' + $userData.firstName
            }
            else {
                $FirstName = $userData.firstName
            }
            if ($lastCheck -ne 'ZZ') {
                $LastName = 'ZZ' + $userData.lastName
            }
            else {
                $FirstName = $userData.lastName
            }
        }
        else {
            $FirstName = 'ZZ-MISSING'
            $LastName = 'ZZ-NAME'
        }
        if (($userData.active -ne $false) -and ($userData.userProfileName -ne 'Offboarded Users')) {
            try {
                Set-Five9User `
                    -Identity $Username `
                    -Active $False `
                    -CanChangePassword $False `
                    -MustChangePassword $False `
                    -FirstName $FirstName `
                    -LastName $LastName `
                    -UserProfileName 'Offboarded Users' | Out-Null
                $ReportList.add([PSCustomObject]@{
                    'Disabled' = 'SUCCESS'
                }) | Out-Null
            }
            catch {
                $_
                $ReportList.add([PSCustomObject]@{
                    'Disabled' = 'FAILURE'
                }) | Out-Null
            }
        }
        elseif ($userData.active -ne $false) {
            try {
                Set-Five9User `
                    -Identity $Username `
                    -Active $False `
                    -CanChangePassword $False `
                    -MustChangePassword $False `
                    -FirstName $FirstName `
                    -LastName $LastName | Out-Null
                $ReportList.add([PSCustomObject]@{
                    'Disabled' = 'SUCCESS'
                }) | Out-Null
            }
            catch {
                $_
                $ReportList.add([PSCustomObject]@{
                    'Disabled' = 'FAILURE'
                }) | Out-Null
            }
        }
        else {
            Write-Host "$username was already disabled."
            $ReportList.add([PSCustomObject]@{
                'Disabled' = 'ALREADY DISABLED'
            }) | Out-Null
        }

        $AgentGroupReport = New-Object System.Collections.ArrayList($null)
        ForEach ($group in $agentGroups) {
            try {
                Write-Host "Removing $Username from $group..."
                Remove-Five9AgentGroupMember -Members $Username -Name $group
                [void]$AgentGroupReport.add([PSCustomObject]@{
                    'Removed' = 'SUCCESS'
                    'Group' = $group
                })
                Start-Sleep -Milliseconds 250 #Rate limiting to prevent timeout error
            }
            catch {
                $_
                [void]$AgentGroupReport.add([PSCustomObject]@{
                    'Removed' = 'FAILURE'
                    'Group' = $group
                })
            }
        }
        if ($null = $AgentGroupReport) {
            $ReportList | Add-Member -MemberType NoteProperty -Name 'Agent Group Removed' -Value @('NO MATCHES')
        }
        elseif ($AgentGroupReport.Removed -notcontains 'FAILURE') {
                $ReportList | Add-Member -MemberType NoteProperty -Name 'Agent Group Removed' -Value @($AgentGroupReport.Group, 'SUCCESS')
        }
        else {
            $groupSuccessFilter = ($AgentGroupReport | ForEach-Object {If ($_.Removed -eq 'SUCCESS') {$_}})
            $groupFailureFilter = ($AgentGroupReport | ForEach-Object {If ($_.Removed -eq 'FAILURE') {$_}})
            $ReportList | Add-Member -MemberType NoteProperty -Name 'Agent Group Removed' -Value @($groupSuccessFilter.Group, 'SUCCESS', $groupFailureFilter.Group, 'FAILURE')
        }        
    }
    else {
        Write-Host "$Username wasn't found in $Domain."
        $ReportList.add([PSCustomObject]@{
            'Disabled' = 'NOT FOUND'
            'Agent Group Removed' = 'NOT FOUND'
        }) | Out-Null
    }
    
    #Remove matching Speed Dial
    $SpeedDialFilter = ($SpeedDials | ForEach-Object {If ($_.Number -eq $DNIS) {$_}})
    if ($SpeedDialFilter){
        ForEach ($SpeedDial in $SpeedDialFilter) {
            Write-Host "Removing $DNIS From $Domain Speed Dial..."
            try {
                Remove-Five9SpeedDialNumber $SpeedDial.Code | Out-Null
                $ReportList | Add-Member -MemberType NoteProperty -Name 'Speed Dial Removed' -Value @($SpeedDial.Code, 'SUCCESS')
            }
            catch {
                $_
                $ReportList | Add-Member -MemberType NoteProperty -Name 'Speed Dial Removed' -Value @($SpeedDial.Code, 'FAILED')
            }
        }   
    }
    else {
        $ReportList | Add-Member -MemberType NoteProperty -Name 'Speed Dial Removed' -Value @($SpeedDial.Code, 'NOT FOUND')
    }

    #Remove matching Contact Record
    $Contact = @{number1 = $ContactNumber}
    $SearchContact = Get-Five9ContactRecord -LookupCriteria $Contact
    if($SearchContact){
        $DeleteRecord = New-Object System.Collections.ArrayList($null)
        $DeleteRecord.add([PSCustomObject]@{
            'number1' = $ContactNumber
        }) | Out-Null
        Write-Host "Removing $ContactNumber from $Domain contacts..."
        try {
            Remove-Five9ContactRecord -InputObject $DeleteRecord | Out-Null
            $ReportList | Add-Member -MemberType NoteProperty -Name 'Contact Record' -Value 'SUCCESS'
        }
        catch {
            $_
            $ReportList | Add-Member -MemberType NoteProperty -Name 'Contact Record' -Value 'FAILURE'
        }
    }
    else {
        Write-Host "No matching contact record found. (ఠ _ ఠ) Strange..."
        $ReportList | Add-Member -MemberType NoteProperty -Name 'Contact Record' -Value 'NOT FOUND'
    }
    return $ReportList
}


function Step-Five9CleanupFive9 ($Record) {
    $ReportList = New-Object System.Collections.ArrayList($null) #Stores the status of each step for report summary
    
    $Username = $Record.Username

    $userData = Get-Five9User $Username #Get agent user data
    $agentGroups = $userData.agentGroups #Create list of agent groups to be removed from

    if ($userData.fullName -ne ('' -or $null)) {
        $firstCheck = $userData.firstName.substring(0, 2) #Checks the first two characters of a string to see if it's already ZZ
        $lastCheck = $userData.lastName.substring(0, 2) #Checks the first two characters of a string to see if it's already ZZ
        if ($firstCheck -ne 'ZZ') {
            $FirstName = 'ZZ' + $userData.firstName
        }
        else {
            $FirstName = $userData.firstName
        }
        if ($lastCheck -ne 'ZZ') {
            $LastName = 'ZZ' + $userData.lastName
        }
        else {
            $FirstName = $userData.lastName
        }
    }
    else {
        $FirstName = 'ZZ-MISSING'
        $LastName = 'ZZ-NAME'
    }
    if (($userData.active -ne $false) -and ($userData.userProfileName -ne 'Offboarded Users')) {
        try {
            Set-Five9User `
                -Identity $Username `
                -Active $False `
                -CanChangePassword $False `
                -MustChangePassword $False `
                -FirstName $FirstName `
                -LastName $LastName `
                -UserProfileName 'Offboarded Users' | Out-Null
            $ReportList.add([PSCustomObject]@{
                'Disabled' = 'SUCCESS'
            }) | Out-Null
        }
        catch {
            $_
            $ReportList.add([PSCustomObject]@{
                'Disabled' = 'FAILURE'
            }) | Out-Null
        }
    }
    elseif ($userData.active -ne $false) {
        try {
            Set-Five9User `
                -Identity $Username `
                -Active $False `
                -CanChangePassword $False `
                -MustChangePassword $False `
                -FirstName $FirstName `
                -LastName $LastName | Out-Null
            $ReportList.add([PSCustomObject]@{
                'Disabled' = 'SUCCESS'
            }) | Out-Null
        }
        catch {
            $_
            $ReportList.add([PSCustomObject]@{
                'Disabled' = 'FAILURE'
            }) | Out-Null
        }
    }
    else {
        Write-Host "$username was already disabled."
        $ReportList.add([PSCustomObject]@{
            'Disabled' = 'ALREADY DISABLED'
        }) | Out-Null
    }

    $AgentGroupReport = New-Object System.Collections.ArrayList($null)
    ForEach ($group in $agentGroups) {
        try {
            Write-Host "Removing $Username from $group..."
            Remove-Five9AgentGroupMember -Members $Username -Name $group
            [void]$AgentGroupReport.add([PSCustomObject]@{
                'Removed' = 'SUCCESS'
                'Group' = $group
            })
            Start-Sleep -Milliseconds 250 #Rate limiting to prevent timeout error
        }
        catch {
            $_
            [void]$AgentGroupReport.add([PSCustomObject]@{
                'Removed' = 'FAILURE'
                'Group' = $group
            })
        }
    }
    if ($null = $AgentGroupReport) {
        $ReportList | Add-Member -MemberType NoteProperty -Name 'Agent Group Removed' -Value @($agentGroups, 'NO MATCHES')
    }
    elseif ($AgentGroupReport.Removed -notcontains 'FAILURE') {
        $ReportList | Add-Member -MemberType NoteProperty -Name 'Agent Group Removed' -Value @($agentGroups, 'SUCCESS')
    }
    else {
        $groupSuccessFilter = ($AgentGroupReport | ForEach-Object {If ($_.Removed -eq 'SUCCESS') {$_}})
        $groupFailureFilter = ($AgentGroupReport | ForEach-Object {If ($_.Removed -eq 'FAILURE') {$_}})
        $ReportList | Add-Member -MemberType NoteProperty -Name 'Agent Group Removed' -Value @($groupSuccessFilter.Group, 'SUCCESS', $groupFailureFilter.Group, 'FAILURE')
    }
    return $ReportList     
}

function Step-DNISProcessing ($Records) {
    $DNISList = New-Object System.Collections.ArrayList($null) #Stores a list of all numbers to be moved in Senior
    ForEach ($Record in $Records) {
        if (($Record.DNIS -eq '') -and ($Record.TempDNIS -eq '') -and ($Record.Loopback -eq '')) {
            Continue
        }
        
        if (($Record.Domain -eq 'Senior') -and ($Record.MatchType -eq 'Google')) {
            #Determine if Tiburon user or not
            if (($Username -match 'tiburon')) {
                $DNISList.add([PSCustomObject]@{
                    'DNIS' = $Record.DNIS
                    'TempDNIS' = $Record.TempDNIS
                    'Loopback' = $Record.Loopback
                    'Destination Campaign' = 'Deactivated Agents - Tiburon'
                }) | Out-Null
            }
            else {
                $DNISList.add([PSCustomObject]@{
                    'DNIS' = $Record.DNIS
                    'TempDNIS' = $Record.TempDNIS
                    'Loopback' = $Record.Loopback
                    'Destination Campaign' = 'Deactivated Agents - SelectQuote'
                }) | Out-Null
            }
        }
        if (($Record.Domain -eq 'UHC') -and ($Record.MatchType -eq 'Google')) {
            $DNISList.add([PSCustomObject]@{
                'DNIS' = $Record.DNIS
                'TempDNIS' = $Record.TempDNIS
                'Loopback' = $Record.Loopback
                'Destination Campaign' = 'Agent Direct Dial - Deactivated Agents - UHC'
            }) | Out-Null
        }
        if (($Record.Domain -eq 'CCA') -and ($Record.MatchType -eq 'Google')) {
            $DNISList.add([PSCustomObject]@{
                'DNIS' = $Record.DNIS
                'TempDNIS' = $Record.TempDNIS
                'Loopback' = $Record.Loopback
                'Destination Campaign' = 'CCA - Deactivated Agents'
            }) | Out-Null
        }
        if (($Record.Domain -eq 'SQAH') -and ($Record.MatchType -eq 'Google')) {
            $DNISList.add([PSCustomObject]@{
                'DNIS' = $Record.DNIS
                'TempDNIS' = $Record.TempDNIS
                'Loopback' = $Record.Loopback
                'Destination Campaign' = 'AH - Deactivated Agents'
            }) | Out-Null
        }
        if (($Record.Domain -eq 'Health') -and ($Record.MatchType -eq 'Google')) {
            $DNISList.add([PSCustomObject]@{
                'DNIS' = $Record.DNIS
                'TempDNIS' = $Record.TempDNIS
                'Loopback' = $Record.Loopback
                'Destination Campaign' = 'CSA - Deactivated Agents'
            }) | Out-Null
        }
        if (($Record.Domain -eq 'Life') -and ($Record.MatchType -eq 'Google')) {
            $DNISList.add([PSCustomObject]@{
                'DNIS' = $Record.DNIS
                'TempDNIS' = $Record.TempDNIS
                'Loopback' = $Record.Loopback
                'Destination Campaign' = 'IB - Deactivated Agent - LHA'
            }) | Out-Null
        }
    }
    return $DNISList
}