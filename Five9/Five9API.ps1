function Connect-Five9AdminWebService {
    [CmdletBinding(PositionalBinding=$true)]

    param (
        [Parameter(Mandatory=$false)][PSCredential]$Credential = (Get-Credential),
        [Parameter(Mandatory=$false)][string]$Version = '11',
        [Parameter(Mandatory=$false)][switch]$PassThru = $false
    )

    try {
        try {
            [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
        }
        catch {}

        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

        $wsdl = "https://api.five9.com/wsadmin/v$($Version)/AdminWebService?wsdl&user=$($Credential.Username)"
        Write-Verbose "Connecting to: $($wsdl)"

        $global:DefaultFive9AdminClient = New-WebServiceProxy -Uri $wsdl -Namespace "PSFive9Admin" -Class "PSFive9Admin" -ErrorAction: Stop

        $global:DefaultFive9AdminClient.Credentials = $Credential

        $global:DefaultFive9AdminClient | Add-Member -MemberType NoteProperty -Name Five9DomainName -Value $null -Force
        $global:DefaultFive9AdminClient | Add-Member -MemberType NoteProperty -Name Five9DomainId -Value $null -Force
    }

    catch {
        throw "Error creating web service proxy to Five9 admin web service. $($_.Exception.Message)"
        return
    }
    
    #Test credentials
    try {
        $vccConfig = $global:DefaultFive9AdminClient.getVCCConfiguration()
        Write-Verbose "Connection established to domain id $($vccConfig.domainId) ($($vccConfig.domainName))"

        $global:DefaultFive9AdminClient.Five9DomainName = $vccConfig.domainName
        $global:DefaultFive9AdminClient.Five9DomainId = $vccConfig.domainId

    }
    catch {
        throw "Error connecting to Five9 admin web service. Please check your credentials and try again. $($_.Exception.Message)"
        return
    }

    if ($PassThru -eq $true) {
        return $global:DefaultFive9AdminClient
    }
    return
}

function Test-Five9Connection {
    [CmdletBinding(PositionalBinding=$false)]
    param ()
    if ($global:DefaultFive9AdminClient.Five9DomainName.Length -gt 0) {
        return
    }
    throw "You are not currently connected to the Five9 Admin Web Service. You must first connect using Connect-Five9AdminWebService."
    return
}

function Add-Five9CampaignDNIS {
    [CmdletBinding(PositionalBinding=$true)]
    param ( 
        # Inbound campaign name that a single 10 digit DNIS, or multiple DNISes will be added to
        [Parameter(Mandatory=$true)][Alias('Name')][string]$CampaignName,

        # Single 10 digit DNIS, or array of multiple DNISes to be added to an inbound campaign
        [Parameter(Mandatory=$true)][ValidatePattern('^\+?[0-9]{10,20}$')][string[]]$DNIS
    )
    try {
        Test-Five9Connection -ErrorAction: Stop

        Write-Verbose "$($MyInvocation.MyCommand.Name): Adding DNIS to campaign '$CampaignName'." 
        return $global:DefaultFive9AdminClient.addDNISToCampaign($CampaignName, $DNIS)

    }
    catch {
        throw {
            $_		
            $_ | Write-Error
        }
    }
}

function Remove-Five9CampaignDNIS {
    [CmdletBinding(PositionalBinding=$true)]
    param ( 
        # Inbound campaign name that a single 10 digit DNIS, or multiple DNISes will be removed from
        [Parameter(Mandatory=$true)][Alias('Name')][string]$CampaignName,

        # Single 10 digit DNIS, or array of multiple DNISes to be removed from an inbound campaign
        [Parameter(Mandatory=$true)][ValidatePattern('^\+?[0-9]{10,20}$')][string[]]$DNIS
    )

    try {
        Test-Five9Connection -ErrorAction: Stop

        Write-Verbose "$($MyInvocation.MyCommand.Name): Removing DNIS from campaign '$CampaignName'." 
        return $global:DefaultFive9AdminClient.removeDNISFromCampaign($CampaignName, $DNIS)
    }
    catch {
        throw {
            $_		
            $_ | Write-Error
        }
    }
}
function Get-Five9SpeedDial{
    [CmdletBinding()]
    param ()
    
    try{
        Test-Five9Connection -ErrorAction: Stop

        Write-Verbose "$($MyInvocation.MyCommand.Name): Returning all speed dial numbers." 
        return $global:DefaultFive9AdminClient.getSpeedDialNumbers() | Sort-Object code

    }
    catch{
        throw {
            $_		
            $_ | Write-Error
        }
    }
}

function Remove-Five9SpeedDialNumber{
    [CmdletBinding(PositionalBinding=$true)]
    param(
        # Code assigned to the speed dial number
        [Parameter(Mandatory=$true)][string]$Code
    )

    try{
        Test-Five9Connection -ErrorAction: Stop

        Write-Verbose "$($MyInvocation.MyCommand.Name): Removing speed dial number with code '$Code'." 
        return $global:DefaultFive9AdminClient.removeSpeedDialNumber($Code)

    }
    catch{
        throw {
            $_		
            $_ | Write-Error
        }
    }
}

function Remove-Five9User{
    [CmdletBinding(PositionalBinding=$true)]
    param (
        # Username of user to be removed
        [Parameter(Mandatory=$true)][string]$Username
    )

    try {
        Test-Five9Connection -ErrorAction: Stop

        Write-Verbose "$($MyInvocation.MyCommand.Name): Removing user '$Username'." 
        $response = $global:DefaultFive9AdminClient.deleteUser($Username)

        return $response
    }
    catch {
        throw {
            $_		
            $_ | Write-Error
        }
    }
}

function Set-Five9User{
    [CmdletBinding(PositionalBinding=$false)]
    param (
        [Parameter(Mandatory=$true, Position=0)][string]$Identity, #Username of the user being modified.
        [Parameter(Mandatory=$false)][string]$FirstName, #New first name
        [Parameter(Mandatory=$false)][string]$LastName, #New last name
        [Parameter(Mandatory=$false)][ValidatePattern('^\S{2,}@\S{2,}\.\S{2,}$')][string]$Email, #New email address
        [Parameter(Mandatory=$false)][string]$Password, #New password
        [Parameter(Mandatory=$false)][string]$FederationId, #New federationId. Used for single-sign-on
        [Parameter(Mandatory=$false)][bool]$Active, #Whether the account is enabled
        [Parameter(Mandatory=$false)][bool]$CanChangePassword, #Whether the user can change their password
        [Parameter(Mandatory=$false)][bool]$MustChangePassword, #Whether the user can change their password
        [Parameter(Mandatory=$false)][string]$UserProfileName, #Profile assigned to user
        [Parameter(Mandatory=$false)][dateTime]$StartDate, #Date that user stared using Five9. Used in reporting
        [Parameter(Mandatory=$false)][ValidateLength(1,6)][string]$Extension, #User's phone extension
        [Parameter(Mandatory=$false)][string]$PhoneNumber, #Phone number of the unified communication user
        [Parameter(Mandatory=$false)][string]$Locale, #User's locale
        [Parameter(Mandatory=$false)][string]$UnifiedCommunicationId #Unified communication ID, for example, a Skype for Business ID such as syoung@qa59.local.com
    )

    try{
        Test-Five9Connection -ErrorAction: Stop

        $userToModify = $null
        try {
            $userToModify = $global:DefaultFive9AdminClient.getUsersGeneralInfo($Identity)
        }
        catch{}

        if ($userToModify.Count -gt 1) {
            throw "Multiple user matches were found using query: ""$Identity"". Please try using the exact username of the user you're trying to modify."
            return
        }
        if ($null -eq $userToModify) {
            throw "Cannot find a Five9 user with username: ""$Identity"". Remember that username is case sensitive."
            return
        }

        $userToModify = $userToModify | Select-Object -First 1

        if ($PSBoundParameters.Keys -contains "FirstName") {
            $userToModify.firstName = $FirstName
        }

        if ($PSBoundParameters.Keys -contains "LastName") {
            $userToModify.lastName = $LastName
        }

        if ($PSBoundParameters.Keys -contains "Email") {
            $userToModify.EMail = $Email
        }

        if ($PSBoundParameters.Keys -contains "Password") {
            $userToModify.password = $Password
        }

        if ($PSBoundParameters.Keys -contains "FederationId") {
            $userToModify.federationId = $FederationId
        }

        if ($PSBoundParameters.Keys -contains "CanChangePassword") {
            $userToModify.canChangePasswordSpecified = $true
            $userToModify.canChangePassword = $CanChangePassword
        }

        if ($PSBoundParameters.Keys -contains "MustChangePassword") {
            $userToModify.mustChangePasswordSpecified = $true
            $userToModify.mustChangePassword = $MustChangePassword
        }

        if ($PSBoundParameters.Keys -contains "Active") {
            $userToModify.active = $Active
        }

        if ($PSBoundParameters.Keys -contains "UserProfileName") {
            $userToModify.userProfileName = $UserProfileName
        }

        if ($PSBoundParameters.Keys -contains "StartDate") {
            $userToModify.startDate = $StartDate
        }

        if ($PSBoundParameters.Keys -contains "Extension") {
            $userToModify.extension = $Extension
        }

        if ($PSBoundParameters.Keys -contains "PhoneNumber") {
            $userToModify.phoneNumber = $PhoneNumber
        }

        if ($PSBoundParameters.Keys -contains "Locale") {
            $userToModify.locale = $Locale
        }

        if ($PSBoundParameters.Keys -contains "UnifiedCommunicationId") {
            $userToModify.unifiedCommunicationId = $UnifiedCommunicationId
        }

        Write-Verbose "$($MyInvocation.MyCommand.Name): Modifying user '$Identity'." 
        $response = $global:DefaultFive9AdminClient.modifyUser($userToModify, $null, $null)

        return $response.generalInfo
    }
    catch {
        throw {
            $_		
            $_ | Write-Error
        }
    }
}

function Get-Five9ContactRecord {
    [CmdletBinding(PositionalBinding=$true)]
    param ( 
        # Hashtable containing the criteria used to find matching records in the contact database
        # Note: Hashtable values can use the % symbol for a wildcard. i.e. -LookupCriteria @{number1 = "925%"}
        [Parameter(Mandatory=$true)][hashtable]$LookupCriteria
    )
    try {
        Test-Five9Connection -ErrorAction: Stop

        $crmLookupCriteria = New-Object PSFive9Admin.crmLookupCriteria

        foreach ($key in $LookupCriteria.Keys)
        {
            $criterion = New-Object PSFive9Admin.crmFieldCriterion

            $criterion.field = $key
            $criterion.value = $LookupCriteria[$key]

            $crmLookupCriteria.criteria += $criterion
        }
        $response = $global:DefaultFive9AdminClient.getContactRecords($crmLookupCriteria)

        if ($response.records.Count -lt 1) {
            # no records were found
            return
        }
        else {
            # convert response from Five9 API in pscustom objects
            $returnObjects = @()
            foreach ($record in $response.records) {
                $obj = New-Object psobject
                for ($i = 0; $i -lt $response.fields.Count; $i++) { 
                    $obj | Add-Member -MemberType: NoteProperty -Name $response.fields[$i] -Value $record.values[$i]
                }
                $returnObjects += $obj
            }
            Write-Verbose "$($MyInvocation.MyCommand.Name): Returning $($returnObjects.Count) contact records matching LookupCritera." 
            return $returnObjects
        }
    }
    catch {
        throw {
            $_		
            $_ | Write-Error
        }
    }
}

function Remove-Five9ContactRecord {
    [CmdletBinding(DefaultParametersetName='InputObject', PositionalBinding=$false)]
    param ( 
        # Single object or array of objects to be removed from the contact record database. Note: Parameter not needed when specifying a CsvPath
        [Parameter(ParameterSetName='InputObject', Mandatory=$true)][psobject[]]$InputObject,

        # Local file path to CSV file containing records to be removed from contact record database. Note: Parameter not needed when specifying an InputObject
        [Parameter(ParameterSetName='CsvPath', Mandatory=$true)][string]$CsvPath,

        <#
        Specifies the modes used for deleting data from the contact database
        Options are:
            • DELETE_ALL (Default) - Delete all specified records
            • DELETE_SOLE_MATCHES - Delete only single matches
            • DELETE_EXCEPT_FIRST - Delete all records except the first matching record
        #>
        [Parameter(Mandatory=$false)][string][ValidateSet("DELETE_ALL", "DELETE_SOLE_MATCHES", "DELETE_EXCEPT_FIRST")]$CrmDeleteMode = "DELETE_ALL",

        # Single string, or array of strings which designate key(s). It is used to find matching reocrds in the database to remove.
        # If omitted, 'number1' will be used
        [Parameter(Mandatory=$false)][string[]]$Key = @("number1"),

        <#
        Whether to stop the removal if incorrect data is found
        For example, if set to True and you have a column named hair_color in your data, but that field has not been created as a contact field, the function will fail
        Options are:
            • True: The record is rejected when at least one field fails validation
            • False: Default. The record is accepted. However, changes to the fields that fail validation are rejected
        #>
        [Parameter(Mandatory=$false)][bool]$FailOnFieldParseError,

        # Notification about results is sent to the email addresses that you set for your application
        [Parameter(Mandatory=$false)][string]$ReportEmail
    )

    try {
        Test-Five9Connection -ErrorAction: Stop

        if ($PSCmdlet.ParameterSetName -eq 'InputObject')
        {
            $csv = $InputObject | ConvertTo-Csv -NoTypeInformation
        }
        elseif ($PSCmdlet.ParameterSetName -eq 'CsvPath')
        {
            # try to import csv file so that if it throw an error, we know the data is bad
            $csv = Import-Csv $CsvPath | ConvertTo-Csv -NoTypeInformation
        }
        else
        {
            # should never reach this point becasue user should use either InputObject or CsvPath
            return
        }

        $headers = $csv[0] -replace '"' -split ','

        # verify that key(s) passed are present in $Inputobject
        foreach ($k in $Key)
        {
            if ($headers -notcontains $k)
            {
                throw "Specified key ""$k"" is not a property name found in data being imported."
                return
            }
        }

        $crmDeleteSettings = New-Object PSFive9Admin.crmDeleteSettings

        # prepare "fieldMapping" per Five9's documentation
        $counter = 1
        foreach ($header in $headers) {
            $isKey = $false
            if ($Key -contains $header) {
                $isKey = $true
            }

            $crmDeleteSettings.fieldsMapping += @{
                columnNumber = $counter
                fieldName = $header
                key = $isKey
            }
            $counter++
        }

        $csvData = ($csv | select -Skip 1) | Out-String

        $crmDeleteSettings.crmDeleteModeSpecified = $true
        $crmDeleteSettings.crmDeleteMode = $CrmDeleteMode
    
        if ($PSBoundParameters.Keys -contains "FailOnFieldParseError") {
            $crmDeleteSettings.failOnFieldParseErrorSpecified = $true
            $crmDeleteSettings.failOnFieldParseError = $FailOnFieldParseError
        }

        if ($PSBoundParameters.Keys -contains "ReportEmail") {
            $crmDeleteSettings.reportEmail = $ReportEmail
        }
        Write-Verbose "$($MyInvocation.MyCommand.Name): Removing contact records from database." 

        # single record
        if ($InputObject.Count -eq 1) {
            $data = $csvData -replace '"' -split ','
            $response = $global:DefaultFive9AdminClient.deleteFromContacts($crmDeleteSettings, $data)  
        }
        else {
            $response = $global:DefaultFive9AdminClient.deleteFromContactsCsv($crmDeleteSettings, $csvData)
        }
        return $response
    }
    catch {
        throw {
            $_		
            $_ | Write-Error
        }
    }
}

function Start-Five9Report {
    [CmdletBinding(PositionalBinding=$false)]
    param(
        # Folder where report is located (case sensitive). i.e. "Call Log Reports"
        [Parameter(Mandatory=$true, Position=0)][string]$FolderName,

        # Report Name (case sensitive). i.e. "Call Log"
        [Parameter(Mandatory=$true, Position=1)][string]$ReportName,

        # Start of the reporting period with the time zone. i.e. 2019-04-23T21:00:00.000-07:00
        # If parameter is omitted, start date will be set to 7 days ago
        [Parameter(Mandatory=$false)][datetime]$StartDateTime = ((Get-Date).AddDays(-7)),

        # End of the reporting period with the time zone. i.e. 2019-05-23T21:00:00.000-07:00
        # If parameter is omitted, the current date time will be used.
        [Parameter(Mandatory=$false)][datetime]$EndDateTime = (Get-Date)
    )

    try {
        Test-Five9Connection -ErrorAction: Stop

        $customReportCriteria = New-Object PSFive9Admin.customReportCriteria

        $customReportCriteria.time = New-Object PSFive9Admin.reportTimeCriteria
        $customReportCriteria.time.start = $StartDateTime
        $customReportCriteria.time.startSpecified = $true
        $customReportCriteria.time.end = $EndDateTime
        $customReportCriteria.time.endSpecified = $true

        <# report is better filtered in the reporting GUI
        $reportObject = New-Object PSFive9Admin.reportObjectList
        $reportObject.objectNames = 'Inbound-New'
        $reportObject.objectType = 'Campaign'
        $reportObject.objectTypeSpecified = $true
        $customReportCriteria.reportObjects += $reportObject
        #>

        Write-Verbose "$($MyInvocation.MyCommand.Name): Starting report '$FolderName\$ReportName'" 

        $id = $global:DefaultFive9AdminClient.runReport($FolderName, $ReportName, $customReportCriteria)

        return $id

    }
    catch {
        throw {
            $_		
            $_ | Write-Error
        }
    }
}

function Get-Five9ReportResult {
    [CmdletBinding(PositionalBinding=$true)]
    param (
        # Unique identifier returned by Start-Five9Report
        [Parameter(Mandatory=$true)][string]$Identifier,

         # Seconds to retry report retrieval, defaults to 5 if not specified
        [Parameter(Mandatory=$false)][int]$WaitSeconds = 5
    )

    try {
        Test-Five9Connection -ErrorAction: Stop
        $reportIsRunning = $true

        while ($reportIsRunning -eq $true) {
            $reportStatus = $null
            $reportStatus = $DefaultFive9AdminClient.isReportRunning($Identifier, "")

            if ($reportStatus -eq $false) {
                $reportIsRunning = $false
            }
            else {
                Write-Verbose "$($MyInvocation.MyCommand.Name): Report is still running. Waiting $WaitSeconds seconds..."
                Start-Sleep -Seconds $WaitSeconds
            }
        }
    
        $data = $global:DefaultFive9AdminClient.getReportResultCsv($Identifier)
        $objects = $data | ConvertFrom-Csv -ErrorAction: SilentlyContinue

        if ($objects.Count -gt 0) {
            Write-Verbose "$($MyInvocation.MyCommand.Name): Returning report with $($objects.Count) rows."
            return $objects
        }
        else {
            Write-Verbose "$($MyInvocation.MyCommand.Name): Report did not return any data."
            return $objects
        }

    }
    catch {
        throw {
            $_		
            $_ | Write-Error
        }
    }
}

function Remove-Five9AgentGroupMember {
    [CmdletBinding(PositionalBinding=$true)]
    param ( 
        # Name of agent group to remove member(s) from
        [Parameter(Mandatory=$true)][Alias('Name')][string]$GroupName,

        # Username of single member, or array of multiple usernames to be removed from agent group
        [Parameter(Mandatory=$true)][string[]]$Members
    )

    try {
        Test-Five9Connection -ErrorAction: Stop

        $agentGroupToModify = $null
        try {
            $agentGroupToModify = $global:DefaultFive9AdminClient.getAgentGroup($GroupName)
        }
        catch {}

        if ($agentGroupToModify.Count -gt 1) {
            throw "Multiple Agent Groups were found using query: ""$GroupName"". Please try using the exact username of the user you're trying to modify."
            return
        }

        if ($agentGroupToModify -eq $null) {
            throw "Cannot find a Agent Group with name: ""$GroupName"". Remember that Name is case sensitive."
            return
        }

        Write-Verbose "$($MyInvocation.MyCommand.Name): Removing member(s) to agent group '$GroupName'." 
        $response =  $global:DefaultFive9AdminClient.modifyAgentGroup($agentGroupToModify, $null, $Members)

        return $response
    }
    catch {
        throw {
            $_		
            $_ | Write-Error
        }
    }
}