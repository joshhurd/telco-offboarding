#!/usr/bin/env python3
import re, os

#########################################
#THIS NEEDS TO BE CONVERTED TO POWERSHELL
#########################################

#This script will interpret text blocks from Agent Change Alert tickets and return employee names.

#Get file path
file1 = input("Enter filepath: ")
fList = [] #This list is used to return all found names in file

#Search file for a regular expression match and pull it as a string
def searchMatch(file):
    try:
        with open(file, 'r') as text:
            convertString = text.read()
            strippedString = convertString.replace('\n', " ")
            #Separate merged tickets using a regular expression
            match = re.findall(r'To:([\s\S]{0,800})[0-9]{1,2}/[0-9]{2}/[0-9]{2,4} [0-9]{1,2}:[0-9]{1,2} [AP]M', strippedString)
            return match
    except:
        print('Did not find expected ticket format. Is this the correct file?')

def runMatches(file):
    for entry in searchMatch(file):
        tFlag = re.search(r'Termination Flag: Yes', entry)
        if bool(tFlag) == True:
            employee = re.findall(r"Employee Name: ([A-z']{1,25} [A-z ]{0,2}[A-z']{1,25}).*Requester", entry)
            for item in employee:
                itemReplace = item.replace(' ',' ')
                itemLower = itemReplace.lower()
                fList.append(itemLower)
            ##Might use these later for reporting##
            #division = re.findall(r"Division Name: ([A-z]{1,15}).*Employee", entry)
            #changeID = re.findall(r"Change ID: ([0-9A-z\-]{20,40}).*History", entry)
        else:
            continue

def GetNames(file):
    runMatches(file)
    dupRemove = list(set(fList)) #Remove duplicate entries in list
    return dupRemove

outFile = GetNames(file1)

output_filePath = os.path.realpath(os.path.join(os.getcwd(), 'telco-offboarding//agentlist.txt'))

with open(output_filePath, "w") as f:
    for line in outFile:
        f.write(line)
        f.write('\n')
    f.close()