#!/usr/bin/env powershell

#This is a tool to disable a user in a given domain.

Connect-Five9AdminWebService

#Prompt for CSV file
Write-Host "Please select .csv file from template:"
Add-Type -AssemblyName System.Windows.Forms
$FileBrowser = New-Object System.Windows.Forms.OpenFileDialog -Property @{ 
    InitialDirectory = [Environment]::GetFolderPath('Desktop') 
    Filter = 'SpreadSheet (*.csv)|*.csv'
}
$null = $FileBrowser.ShowDialog()
$csvFile = $FileBrowser.FileName
$data = Import-Csv $csvFile

#Iterate through CSV file and disable each user
ForEach ($line in $data) {
    $counter++
    $banner = 'Disabling users...'
    Write-Progress -Activity $banner -CurrentOperation $line -PercentComplete (($counter / $data.Count) * 100)
    Set-Five9User `
        -Identity $line.Username `
        -Active $False `
        -CanChangePassword $False `
        -MustChangePassword $False
}
