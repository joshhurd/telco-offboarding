#!/usr/bin/env powershell

#This tool will use a provided list of usernames and delete them from the domain

Connect-Five9AdminWebService

Write-Host "Please select .csv file from template:"
Add-Type -AssemblyName System.Windows.Forms
$FileBrowser = New-Object System.Windows.Forms.OpenFileDialog -Property @{ 
    InitialDirectory = [Environment]::GetFolderPath('Desktop') 
    Filter = 'SpreadSheet (*.csv)|*.csv'
}
$null = $FileBrowser.ShowDialog()

$csvFile = $FileBrowser.FileName

Import-Csv $csvFile | ForEach-Object {
    $username = ($_.Username)

    Remove-Five9user $username

    Write-Host $username -NoNewline
    Write-Host " deleted." -ForegroundColor Yellow
}