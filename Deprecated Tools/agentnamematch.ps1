#!/usr/bin/env powershell

#This is a tool to match a list of names with user in a given domain.

function Step-GetFive9Users {
    #Grab Five9 user list report
    Write-Host "Fetching user report..." -ForegroundColor Yellow

    $id = Start-Five9Report `
        -FolderName "API Reports" `
        -ReportName "Agents Information"

    $users = Get-Five9ReportResult `
        -Identifier $id `
        -WaitSeconds 5

    return $users
}

Connect-Five9AdminWebService

#Prompt for CSV file
Write-Host "Please select .csv file from template:"
Add-Type -AssemblyName System.Windows.Forms
$FileBrowser = New-Object System.Windows.Forms.OpenFileDialog -Property @{ 
    InitialDirectory = [Environment]::GetFolderPath('Desktop') 
    Filter = 'SpreadSheet (*.csv)|*.csv'
}
$null = $FileBrowser.ShowDialog()

$csvFile = $FileBrowser.FileName
$results = Step-GetFive9Users
$agentList = @()

Import-Csv $csvFile | ForEach-Object {
    $fullName = ($_.Name)

    $filterResults = $results | Where-Object {$_.'AGENT NAME' -eq $fullName}
    $agentResults = $filterResults.AGENT

    if ($agentResults) {
        Write-Host "The following usersnames were matched with " -NoNewline -ForegroundColor Green
        Write-Host $fullName -NoNewline
        Write-Host ":" -ForegroundColor Green
        Write-Host ($agentResults | Out-String)

        $agentList += $agentResults
    }
}

$currentDirectory = Get-Location
$agentList | Out-File ".\AgentUsernameList.csv"

Write-Host "The following usernames were matched and written to " -NoNewline -ForegroundColor Yellow
Write-Host $currentDirectory -NoNewline
Write-Host "\AgentUsernameList.csv"

Write-Host ($agentList | Out-String)